local o = vim.opt_local
o.autoindent = true
o.expandtab = true
o.softtabstop = 2
o.tabstop = 2
o.indentkeys:remove(':')
