local o = vim.opt_local
o.wrap = true
o.number = false
o.relativenumber = false
