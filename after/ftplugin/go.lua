local o = vim.opt_local
o.list = false
o.softtabstop = 2
o.tabstop = 2
o.expandtab = false
