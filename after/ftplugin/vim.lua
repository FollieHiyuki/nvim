local o = vim.opt_local
o.colorcolumn = { 120 }
o.iskeyword = vim.opt.iskeyword + ':' + '#'
o.tags = vim.opt.tags + '$DATA_PATH/tags'
