local o = vim.opt_local
o.expandtab = false
o.softtabstop = 2
o.tabstop = 2
