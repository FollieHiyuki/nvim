local o = vim.opt_local
o.autoindent = false
o.expandtab = true
o.softtabstop = 2
o.tabstop = 2
