local styles = {
    single = {
        topleft = '┌',
        topright = '┐',
        top = '─',
        left = '│',
        right = '│',
        botleft = '└',
        botright = '┘',
        bot = '─',
    },
    rounded = {
        topleft = '╭',
        topright = '╮',
        top = '─',
        left = '│',
        right = '│',
        botleft = '╰',
        botright = '╯',
        bot = '─',
    },
    double = {
        topleft = '╔',
        topright = '╗',
        top = '═',
        left = '║',
        right = '║',
        botleft = '╚',
        botright = '╝',
        bot = '═',
    },
}

return {
    summary = {
        min_coverage = 75.0,
        width_percentage = 0.8,
        borders = styles[require('user.config.vars').border],
        window = { winblend = 0 },
    },
}
