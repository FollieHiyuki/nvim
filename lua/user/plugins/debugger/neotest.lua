local neotest_ns = vim.api.nvim_create_namespace('neotest')
vim.diagnostic.config({
    virtual_text = {
        format = function(diagnostic)
            local message = diagnostic.message:gsub('\n', ' '):gsub('\t', ' '):gsub('%s+', ' '):gsub('^%s+', '')
            return message
        end,
    },
    virtual_lines = false,
}, neotest_ns)

require('neotest').setup {
    adapters = {
        require('neotest-go'), -- TODO: https://github.com/nvim-neotest/neotest-go/issues/57 (generate coverage file)
        require('neotest-jest') { cwd = require('neotest-jest').root },
        require('neotest-python'),
        require('neotest-vitest'),
        require('neotest-zig'),
    },
    consumers = {
        overseer = require('neotest.consumers.overseer'),
    },
    discovery = { enabled = false },
    floating = {
        border = require('user.config.vars').border,
        max_height = 0.8,
        max_width = 0.8,
    },
    icons = {
        child_indent = '│',
        child_prefix = '├',
        collapsed = '─',
        expanded = '┐',
        failed = '',
        final_child_indent = ' ',
        final_child_prefix = '└',
        non_collapsible = '─',
        passed = '',
        running = '',
        running_animated = { '/', '|', '\\', '-', '/', '|', '\\', '-' },
        skipped = '󰒬',
        unknown = ''
    },
}
