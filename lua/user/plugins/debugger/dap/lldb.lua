local dap = require('dap')

local ask_for_args = function()
    local args = {}
    vim.ui.input({ prompt = 'Args: ' }, function(input)
        args = vim.split(input or '', ' ')
    end)
    return args
end

local ask_for_executable = function()
    local exec_location = ''
    vim.ui.input({
        prompt = 'Path to executable: ',
        default = vim.fn.getcwd() .. '/',
        completion = 'file',
    }, function(input)
        if input then
            exec_location = input
        end
    end)
    return exec_location
end

dap.adapters.lldb = {
    name = 'lldb',
    type = 'executable',
    command = vim.fn.exepath('lldb-dap'),
}

dap.configurations.zig = {
    {
        name = 'Launch',
        type = 'lldb',
        request = 'launch',
        program = ask_for_executable,
        args = ask_for_args,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
    },
}

dap.configurations.c = dap.configurations.zig
dap.configurations.cpp = dap.configurations.zig
