local dap = require('dap')

local function get_adapter_path()
    local adapter_path = vim.fn.exepath('elixir-debug-adapter')

    if adapter_path == '' then
        return vim.fn.stdpath('data') .. '/programs/elixirls/scripts/debug_adapter.sh'
    end

    return adapter_path
end

dap.adapters.mix_task = {
    type = 'executable',
    command = get_adapter_path(),
}

dap.configurations.elixir = {
    {
        type = 'mix_task',
        name = 'mix test',
        request = 'launch',
        task = 'test',
        taskArgs = { '--trace' },
        startApps = true,
        projectDir = '${workspaceFolder}',
        requireFiles = {
            'test/**/test_helper.exs',
            'test/**/*_test.exs',
        },
    },
    {
        type = 'mix_task',
        name = 'phoenix',
        request = 'launch',
        task = 'phx.server',
        projectDir = '${workspaceFolder}',
    }
}
