local dap = require('dap')

local function get_adapter_executable()
    local adapter_path = vim.fn.exepath('js-debug')
    if adapter_path == '' then
        return {
            command = vim.fn.exepath('node'),
            args = { vim.fn.stdpath('data') .. '/programs/vscode-js-debug/dist/src/dapDebugServer.js', '${port}' },
        }
    end

    return {
        command = adapter_path,
        args = { '${port}' },
    }
end

dap.adapters['pwa-node'] = {
    type = 'server',
    host = 'localhost',
    port = '${port}',
    executable = get_adapter_executable(),
}

dap.configurations.javascript = {
    {
        type = 'pwa-node',
        request = 'launch',
        name = 'Launch file',
        program = '${file}',
        cwd = '${workspaceFolder}',
    },
}

dap.configurations.typescript = {
    {
        type = 'pwa-node',
        request = 'launch',
        name = 'Launch file',
        runtimeExecutable = 'deno',
        runtimeArgs = { 'run', '--inspect-wait', '--allow-all' },
        program = '${file}',
        cwd = '${workspaceFolder}',
        attachSimplePort = 9229,
    },
}
