local vars = require('user.config.vars')

for name, sign in pairs(vars.icons.dap) do
    sign = type(sign) == 'table' and sign or { sign }
    vim.fn.sign_define('Dap' .. name, {
        text = sign[1],
        texthl = sign[2] or 'DiagnosticInfo',
        linehl = sign[3],
        numhl = sign[3],
    })
end

for _, mod in ipairs { 'dlv', 'debugpy', 'lldb', 'elixir', 'pwa-node' } do
    require('user.plugins.debugger.dap.' .. mod)
end

-- Use overseer's JSON decoder
require('dap.ext.vscode').json_decode = require('overseer.json').decode
-- Then enable the integration manually
require('overseer').enable_dap()
