local dap = require('dap')

dap.adapters.python = function(callback, config)
    if config.request == 'attach' then
        local port = (config.connect or config).port
        local host = (config.connect or config).host or '127.0.0.1'
        callback {
            type = 'server',
            port = assert(port, '`connect.port` is required for a python `attach` configuration'),
            host = host,
            options = { source_filetype = 'python' },
        }
        return
    end

    local python_path = vim.fn.exepath('python')
    if vim.fn.exepath('debugpy') == '' then
        python_path = vim.fn.stdpath('data') .. '/programs/debugpy/bin/python'
    end

    callback {
        type = 'executable',
        command = python_path,
        args = { '-m', 'debugpy.adapter' },
        options = { source_filetype = 'python' },
    }
end

dap.configurations.python = {
    {
        name = 'Launch file',
        type = 'python',
        request = 'launch',
        program = '${file}',
        pythonPath = function()
            local cwd, venv, conda = vim.fn.getcwd(), vim.env.VIRTUAL_ENV, vim.env.CONDA_PREFIX
            if venv and vim.fn.executable(venv .. '/bin/python') == 1 then
                return venv .. '/bin/python'
            end
            if conda and vim.fn.executable(conda .. '/bin/python') == 1 then
                return conda .. '/bin/python'
            end
            if vim.fn.executable(cwd .. '/venv/bin/python') == 1 then
                return cwd .. '/venv/bin/python'
            end
            if vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
                return cwd .. '/.venv/bin/python'
            end
            return vim.fn.exepath('python')
        end,
    },
}
