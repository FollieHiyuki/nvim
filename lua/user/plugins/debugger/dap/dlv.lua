local dap = require('dap')
local lazy_util = require('lazy.core.util')

-- Default port to attach to an existing Delve instance
local delve_port = 38697

-- https://github.com/ray-x/go.nvim/blob/master/lua/go/dap.lua
dap.adapters.go = function(callback, config)
    local host = (config.connect or config).host or '127.0.0.1'

    if config.request == 'attach' and config.mode == 'remote' then
        callback {
            type = 'server',
            host = host,
            port = (config.connect or config).port or delve_port,
        }
        return
    end

    -- Randomize a random port number to launch Delve with
    math.randomseed(os.time())
    local port = (config.connect or config).port or (38000 + math.random(1, 1000))

    local stdout = vim.uv.new_pipe(false)
    local stderr = vim.uv.new_pipe(false)
    local handle, pid_or_err

    local function on_read(err, chunk)
        assert(not err, err)
        if not chunk or chunk == '' then
            return
        end

        if chunk:find("couldn't start") then
            vim.schedule(function()
                lazy_util.error(chunk, { title = '[go] DAP Error' })
            end)
        end

        vim.schedule(function()
            require('dap.repl').append(chunk)
        end)
    end

    handle, pid_or_err = vim.uv.spawn('dlv', {
        stdio = { nil, stdout, stderr },
        args = { 'dap', '--listen', string.format('%s:%s', host, port) },
        detached = true,
    }, function(code)
        stdout:close()
        stderr:close()
        handle:close()

        if code ~= 0 then
            vim.schedule(function()
                lazy_util.warn(string.format('Delve exited with exit code: %d', code), { title = '[go] DAP Warning' })
            end)
        end
    end)
    assert(handle, string.format('Error running delve: %s', pid_or_err))
    stdout:read_start(on_read)
    stderr:read_start(on_read)

    -- Wait for delve to start
    vim.defer_fn(function()
        callback { type = 'server', host = host, port = port }
    end, 1000)
end

dap.configurations.go = {
    {
        name = 'Attach',
        mode = 'local',
        type = 'go',
        request = 'attach',
        processId = require('dap.utils').pick_process,
    },
    -- TODO: test this remote attach setup with Bazel
    -- Ref:
    -- - https://github.com/bazelbuild/rules_go/issues/1844#issuecomment-1281231804
    -- - https://github.com/golang/vscode-go/wiki/debugging#remote-debugging
    {
        name = 'Attach (remote)',
        mode = 'remote',
        type = 'go',
        request = 'attach',
        connect = function()
            local response = {}
            vim.ui.input({ prompt = 'Connection: ' }, function(input)
                response = vim.split(input or ('127.0.0.1:' .. delve_port), ':')
            end)
            return { host = response[1], port = response[2] or delve_port }
        end,
    },
    {
        name = 'Debug',
        type = 'go',
        request = 'launch',
        program = '${file}',
    },
    {
        name = 'Debug (arguments)',
        type = 'go',
        request = 'launch',
        program = '${file}',
        args = function()
            local args = {}
            vim.ui.input({ prompt = 'Args: ' }, function(input)
                args = vim.split(input or '', ' ')
            end)
            return args
        end,
    },
    {
        name = 'Debug test',
        type = 'go',
        request = 'launch',
        mode = 'test',
        program = '${file}',
    },
    {
        name = 'Debug test (go.mod)',
        type = 'go',
        request = 'launch',
        mode = 'test',
        program = './${relativeFileDirname}',
    },
}
