return {
    {
        'rcarriga/nvim-dap-ui',
        keys = {
            { '<leader>du', function() require('dapui').toggle() end, desc = 'Dap UI' },
            { '<leader>de', function() require('dapui').eval() end, desc = 'Eval', mode = { 'n', 'v' } },
        },
        dependencies = {
            { 'theHamsta/nvim-dap-virtual-text', config = true },
            {
                'mfussenegger/nvim-dap',
                keys = {
                    { '<leader>dB', function() require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = 'Breakpoint condition' },
                    { '<leader>db', function() require('dap').toggle_breakpoint() end, desc = 'Toggle breakpoint' },
                    { '<leader>dc', function() require('dap').continue() end, desc = 'Continue' },
                    { '<leader>dC', function() require('dap').run_to_cursor() end, desc = 'Run to cursor' },
                    { '<leader>dg', function() require('dap').goto_() end, desc = 'Go to line (skipped)' },
                    { '<leader>di', function() require('dap').step_into() end, desc = 'Step into' },
                    { '<leader>dj', function() require('dap').down() end, desc = 'Down' },
                    { '<leader>dk', function() require('dap').up() end, desc = 'Up' },
                    { '<leader>dl', function() require('dap').run_last() end, desc = 'Run last' },
                    { '<leader>do', function() require('dap').step_out() end, desc = 'Step out' },
                    { '<leader>dO', function() require('dap').step_over() end, desc = 'Step over' },
                    { '<leader>dp', function() require('dap').pause() end, desc = 'Pause' },
                    { '<leader>dr', function() require('dap').repl.toggle() end, desc = 'Toggle REPL' },
                    { '<leader>ds', function() require('dap').session() end, desc = 'Session' },
                    { '<leader>dt', function() require('dap').terminate() end, desc = 'Terminate' },
                    { '<leader>dw', function() require('dap.ui.widgets').hover() end, desc = 'Widgets' },
                },
                config = function()
                    require('user.plugins.debugger.dap')
                end,
            },
        },
        opts = {
            floating = { border = require('user.config.vars').border },
        },
        config = function(_, opts)
            local dap, dapui = require('dap'), require('dapui')
            dapui.setup(opts)

            dap.listeners.after.event_initialized['dapui_config'] = function()
                dapui.open()
            end
            dap.listeners.before.event_terminated['dapui_config'] = function()
                dapui.close()
            end
            dap.listeners.before.event_exited['dapui_config'] = function()
                dapui.close()
            end
        end,
    },
    {
        'nvim-neotest/neotest',
        dependencies = {
            'lawrence-laz/neotest-zig',
            'marilari88/neotest-vitest',
            'nvim-neotest/neotest-jest',
            'nvim-neotest/neotest-go',
            'nvim-neotest/neotest-python',
        },
        keys = {
            {
                '<leader>tt',
                function()
                    local actions = {
                        ['Single position'] = function()
                            require('neotest').run.run()
                        end,
                        ['Current file'] = function()
                            require('neotest').run.run(vim.api.nvim_buf_get_name(0))
                        end,
                        ['Directory'] = function()
                            vim.ui.input({
                                prompt = 'Path: ',
                                default = vim.fn.getcwd(),
                                completion = 'file',
                            }, function(path)
                                if path then
                                    require('neotest').run.run(path)
                                end
                            end)
                        end,
                        ['Nearest debug'] = function()
                            require('neotest').run.run { strategy = 'dap' }
                        end,
                        ['Rerun last position'] = function()
                            require('neotest').run.run_last()
                        end,
                        ['All adapters'] = function()
                            for _, adapter_id in ipairs(require('neotest').state.adapter_ids()) do
                                require('neotest').run.run { suite = true, adapter = adapter_id }
                            end
                        end,
                    }

                    vim.ui.select(vim.tbl_keys(actions), { prompt = 'Run test option' }, function(choice)
                        if choice then actions[choice]() end
                    end)
                end,
                desc = 'Run test',
            },
            { '<leader>ts', function() require('neotest').run.stop() end, desc = 'Stop test' },
            { '<leader>ta', function() require('neotest').run.attach() end, desc = 'Attach to test' },
            { '<leader>ty', function() require('neotest').summary.toggle() end, desc = 'Toggle test summary' },
            { '<leader>to', function() require('neotest').output.open { enter = true, auto_close = true } end, desc = 'Show test output' },
            { '<leader>tO', function() require('neotest').output_panel.toggle() end, desc = 'Toggle test panel' },
        },
        config = function()
            require('user.plugins.debugger.neotest')
        end,
    },
    {
        'andythigpen/nvim-coverage',
        cmd = { 'Coverage', 'CoverageLoad', 'CoverageClear', 'CoverageSummary' },
        keys = {
            {
                '<leader>tc',
                function()
                    local actions = {
                        ['Load coverage'] = function() require('coverage').load(true) end,
                        ['Toggle signs'] = function() require('coverage').toggle() end,
                        ['Clear coverage cache'] = function() require('coverage').clear() end,
                        ['Show summary report'] = function() require('coverage').summary() end,
                    }

                    vim.ui.select(vim.tbl_keys(actions), { prompt = 'Coverage action' }, function(choice)
                        if choice then actions[choice]() end
                    end)
                end,
                desc = 'Coverage',
            }
        },
        opts = function()
            return require('user.plugins.debugger.coverage')
        end,
    },
}
