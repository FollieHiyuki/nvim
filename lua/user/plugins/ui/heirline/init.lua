local conditions = require('heirline.conditions')
local components = require('user.plugins.ui.heirline.components')

local excluded_buftype = { 'nofile', 'prompt', 'help', 'quickfix', 'terminal' }
local excluded_ft = {
    'NvimTree',
    'neo-tree',
    'neo-tree-popup',
    'OverseerForm',
    'TelescopePrompt',
    'Trouble',
    'aerial',
    'aerial-nav',
    'alpha',
    'lazy',
    'oil',
    'qf',
    'toggleterm',
    'undotree',
    'dap-repl',
    'dapui_console',
    'dapui_watches',
    'dapui_stacks',
    'dapui_breakpoints',
    'dapui_scopes',
}

require('heirline').setup {
    statusline = {
        hl = conditions.is_active() and 'StatusLine' or 'StatusLineNC',
        fallthrough = false,
        -- Statusline for special buffers
        {
            condition = function()
                return conditions.buffer_matches {
                    buftype = excluded_buftype,
                    filetype = excluded_ft,
                }
            end,
            components.file_info,
            { provider = '%=' },
            components.file_type,
        },
        -- Default statusline
        {
            { provider = '▊ ', hl = { fg = 'blue' } },
            components.vi_mode,
            components.file_size,
            components.file_info,
            components.dap,
            components.code_action,
            components.macro,
            { provider = '%=' },
            components.overseer,
            components.ruler,
            components.file_format,
            components.file_encoding,
            components.file_type,
            components.git,
        },
    },
    winbar = {
        hl = conditions.is_active() and 'WinBar' or 'WinBarNC',
        fallthrough = false,
        -- Inactive winbar
        {
            condition = function()
                return not conditions.is_active()
            end,
            components.diagnostics,
            { provider = '%=' },
            components.lsp_servers,
            components.linters_formatters,
        },
        -- Regular winbar
        {
            components.aerial,
            { provider = '%=' },
            components.diagnostics,
            components.lsp_servers,
            components.linters_formatters,
        },
    },
    opts = {
        disable_winbar_cb = function(args)
            return conditions.buffer_matches({
                buftype = excluded_buftype,
                filetype = excluded_ft,
            }, args.buf)
        end,
        colors = require('user.themes.' .. vim.g.colors_name .. '.colors'),
    },
}
