local vars = require('user.config.vars')

return {
    input = { enabled = false }, -- avoid loading nvim-cmp needlessly. Also, noice.nvim has nice input UI already
    select = {
        backend = { 'telescope', 'builtin', 'nui' },
        nui = {
            border = { style = vars.border },
            win_options = { winblend = 0 },
        },
        builtin = {
            border = vars.border,
            win_options = { winblend = 0 },
            mappings = {
                ['q'] = 'Close',
                ['<Esc>'] = 'Close',
                ['<CR>'] = 'Confirm',
            },
        },
    },
}
