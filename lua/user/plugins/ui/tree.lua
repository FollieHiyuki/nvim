local vars = require('user.config.vars')

return {
    hijack_cursor = true,
    respect_buf_cwd = true,
    sync_root_with_cwd = true,
    hijack_directories = { enable = false },
    diagnostics = {
        enable = true,
        icons = {
            hint = vars.icons.notify.hint,
            info = vars.icons.notify.info,
            warning = vars.icons.notify.warn,
            error = vars.icons.notify.error,
        },
    },
    update_focused_file = { enable = true, update_root = true },
    filters = {
        custom = {
            '^\\.bzr$',
            '^\\.git$',
            '^\\.hg$',
            '^\\.pijul$',
            '^\\.svn$',
            '^_dars$',
            '^node_modules$',
            '^bazel-out$',
            '^bazel-bin$',
            '^bazel-testlogs$',
            '^plz-out$',
            '^\\.plz-cache$',
            '^\\.plz-http-cache$',
            '^\\.DS_Store'
        },
    },
    git = { ignore = false, timeout = 500 },
    view = { width = 35, side = 'left' },
    renderer = {
        add_trailing = true,
        highlight_git = 'name',
        indent_markers = { enable = true },
        icons = {
            symlink_arrow = ' ➛ ',
            glyphs = {
                default = '',
                symlink = '',
                bookmark = '',
                modified = '●',
                folder = {
                    arrow_closed = '',
                    arrow_open = '',
                    default = '󰉋',
                    open = '󰝰',
                    empty = '',
                    empty_open = '',
                    symlink = '',
                    symlink_open = '',
                },
                git = {
                    unstaged = '',
                    staged = '',
                    unmerged = '',
                    renamed = '',
                    untracked = '',
                    deleted = '',
                    ignored = '',
                },
            },
        },
        special_files = { 'Cargo.toml', 'Makefile', 'README.md', 'package.json' },
    },
    actions = {
        file_popup = {
            open_win_config = {
                border = vars.border,
            },
        },
        open_file = {
            resize_window = false,
            window_picker = {
                exclude = {
                    filetype = {
                        'alpha',
                        'lazy',
                        'aerial',
                        'OverseerForm',
                        'TelescopePrompt',
                        'notify',
                        'undotree',
                        'Trouble',
                        'NvimTree',
                        'neo-tree',
                        'neo-tree-popup',
                        'diff',
                        'qf',
                    },
                    buftype = { 'terminal', 'nofile', 'help' },
                },
            },
        },
    },
}
