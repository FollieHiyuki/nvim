local vars = require('user.config.vars')

return {
    lsp = {
        progress = { enabled = false },
        override = {
            ['cmp.entry.get_documentation'] = true,
            ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
            ['vim.lsp.util.stylize_markdown'] = true,
        },
    },
    presets = {
        command_palette = true,
        long_message_to_split = true,
        inc_rename = {
            cmdline = {
                format = {
                    IncRename = {
                        title = ' Rename ',
                        icon = '',
                    },
                },
            },
        },
    },
    views = {
        mini = { win_options = { winblend = 0 } },
        cmdline_popup = { border = { style = vars.border } },
        confirm = { border = { style = vars.border } },
        popup = { border = { style = vars.border } },
        popupmenu = { border = { style = vars.border } },
        hover = {
            relative = 'cursor',
            border = { style = vars.border },
            position = { row = 2, col = 2 },
        },
    },
    routes = {
        {
            filter = {
                event = 'msg_show',
                find = '%d+L, %d+B',
            },
            view = 'mini',
        },
    },
    format = {
        -- default value without the progress bar
        lsp_progress = {
            '{data.progress.percentage}% ',
            { '{spinner} ', hl_group = 'NoiceLspProgressSpinner' },
            { '{data.progress.title} ', hl_group = 'NoiceLspProgressTitle' },
            { '{data.progress.client} ', hl_group = 'NoiceLspProgressClient' },
        },
    },
}
