local vars = require('user.config.vars')

return {
    {
        'goolord/alpha-nvim',
        event = 'VimEnter',
        opts = function()
            return require('user.plugins.ui.alpha')
        end,
        config = function(_, opts)
            require('alpha').setup(opts)

            -- Disable tabline in Alpha buffer
            local showtabline_val = vim.o.showtabline
            if showtabline_val ~= 0 then
                vim.api.nvim_create_autocmd('User', {
                    pattern = 'AlphaReady',
                    callback = function(event)
                        vim.opt.showtabline = 0
                        vim.api.nvim_create_autocmd('BufUnload', {
                            buffer = event.buf,
                            once = true,
                            callback = function()
                                vim.opt.showtabline = showtabline_val
                            end,
                        })
                    end,
                })
            end
        end,
    },
    {
        'tiagovla/scope.nvim',
        event = 'VeryLazy',
        keys = {
            {
                '<leader>fB',
                function()
                    require('telescope').load_extension('scope')
                    vim.cmd.Telescope { args = { 'scope', 'buffers' } }
                end,
                desc = 'Scoped buffers',
            },
        },
        config = true,
    },
    {
        'nanozuki/tabby.nvim',
        enabled = vim.o.showtabline ~= 0 and vars.tabline_provider == 'tabby',
        event = 'VeryLazy',
        config = function()
            require('user.plugins.ui.tabby')
        end,
    },
    {
        'willothy/nvim-cokeline',
        enabled = vim.o.showtabline ~= 0 and vars.tabline_provider == 'cokeline',
        event = 'VeryLazy',
        keys = {
            { '[b', '<Plug>(cokeline-focus-prev)', desc = 'Previous buffer' },
            { ']b', '<Plug>(cokeline-focus-next)', desc = 'Next buffer' },
            { '<localleader>b', function() require('cokeline.mappings').pick('focus') end, desc = 'Pick buffer' },
            { '<localleader>q', function() require('cokeline.mappings').pick('close') end, desc = 'Close picked buffer' },
        },
        opts = function()
            return require('user.plugins.ui.cokeline')
        end,
    },
    {
        'rebelot/heirline.nvim',
        event = 'UIEnter',
        config = function()
            require('user.plugins.ui.heirline')
        end,
    },
    {
        'nvim-tree/nvim-tree.lua',
        enabled = vars.filetree_provider == 'nvim-tree',
        cmd = { 'NvimTreeToggle', 'NvimTreeFindFileToggle', 'NvimTreeFocus' },
        keys = {
            { '<leader>n', function() require('nvim-tree.api').tree.toggle(false, true) end, desc = 'NvimTree' },
            { '<leader>N', function() require('nvim-tree.api').tree.focus() end, desc = 'NvimTree (focus)' },
        },
        opts = require('user.plugins.ui.tree'),
    },
    {
        'nvim-neo-tree/neo-tree.nvim',
        enabled = vars.filetree_provider == 'neo-tree',
        dependencies = {
            {
                's1n7ax/nvim-window-picker',
                config = function()
                    require('window-picker').setup {
                        show_prompt = false,
                        filter_rules = {
                            bo = {
                                filetype = {
                                    'neo-tree',
                                    'neo-tree-popup',
                                    'notify',
                                    'OverseerForm',
                                    'TelescopePrompt',
                                    'Trouble',
                                    'aerial',
                                    'aerial-nav',
                                    'lazy',
                                    'oil',
                                    'qf',
                                    'toggleterm',
                                    'undotree',
                                    'dap-repl',
                                    'dapui_console',
                                    'dapui_watches',
                                    'dapui_stacks',
                                    'dapui_breakpoints',
                                    'dapui_scopes',
                                },
                                buftype = { 'terminal', 'quickfix', 'prompt' },
                            },
                        },
                    }
                end,
            },
        },
        cmd = 'Neotree',
        keys = {
            { '<leader>n', '<cmd>Neotree toggle show reveal_force_cwd<CR>', desc = 'Toggle Neotree' },
            { '<leader>N', '<cmd>Neotree focus reveal_force_cwd<CR>', desc = 'Focus current buffer in Neotree' },
            { '<leader>gf', '<cmd>Neotree float git_status<CR>', desc = 'File tree' },
        },
        opts = function()
            return require('user.plugins.ui.neotree')
        end,
    },
    {
        'petertriho/nvim-scrollbar',
        event = 'BufReadPost',
        opts = {
            set_highlights = false,
            show_in_active_only = true,
            handle = { blend = 0 },
            handlers = { cursor = false },
            excluded_filetypes = {
                'alpha',
                'lazy',
                'notify',
                'aerial',
                'Trouble',
                'NvimTree',
                'neo-tree',
                'neo-tree-popup',
                'qf',
                'prompt',
                'noice',
                'OverseerForm',
                'TelescopePrompt',
            },
        },
    },
    {
        'folke/which-key.nvim',
        event = 'VeryLazy',
        keys = {
            { '<leader>?', function() require('which-key').show { global = false } end, desc = 'Buffer local keymaps (which-key)' },
        },
        opts = require('user.plugins.ui.which-key'),
    },
    {
        'stevearc/dressing.nvim',
        event = 'VeryLazy',
        init = function()
            ---@diagnostic disable-next-line: duplicate-set-field
            vim.ui.select = function(...)
                require('lazy').load({ plugins = { 'dressing.nvim' } })
                return vim.ui.select(...)
            end
        end,
        opts = require('user.plugins.ui.dressing'),
    },
    {
        'folke/noice.nvim',
        event = 'VeryLazy',
        dependencies = {
            {
                'rcarriga/nvim-notify',
                keys = {
                    {
                        '<leader>uN', function()
                            require('notify').dismiss { silent = true, pending = true }
                        end,
                        desc = 'Delete all notifications',
                    },
                    {
                        '<leader>un', function()
                            require('telescope').load_extension('notify')
                            vim.cmd.Telescope { args = { 'notify' } }
                        end,
                        desc = 'Show all notifications',
                    },
                },
                opts = require('user.plugins.ui.notify')
            },
        },
        keys = {
            { '<S-Enter>', function() require('noice').redirect(vim.fn.getcmdline()) end, mode = 'c', desc = 'Redirect Cmdline' },
            { '<leader>ul', function() require('noice').cmd('last') end, desc = 'Noice Last Message' },
            { '<leader>uh', function() require('noice').cmd('history') end, desc = 'Noice History' },
            { '<leader>ua', function() require('noice').cmd('all') end, desc = 'Noice All' },
            { '<C-f>', function() if not require('noice.lsp').scroll(4) then return '<C-f>' end end, silent = true, expr = true, desc = 'Scroll forward', mode = {'i', 'n', 's'} },
            { '<C-b>', function() if not require('noice.lsp').scroll(-4) then return '<C-b>' end end, silent = true, expr = true, desc = 'Scroll backward', mode = {'i', 'n', 's'}},
        },
        opts = require('user.plugins.ui.noice'),
    },
    {
        'folke/trouble.nvim',
        cmd = 'Trouble',
        keys = {
            {
                '[q',
                function()
                    if require('trouble').is_open() then
                        require('trouble').previous { skip_groups = true, jump = true }
                    else
                        vim.cmd.cprev()
                    end
                end,
                desc = 'Previous trouble/quickfix item',
            },
            {
                ']q',
                function()
                    if require('trouble').is_open() then
                        require('trouble').next { skip_groups = true, jump = true }
                    else
                        vim.cmd.cnext()
                    end
                end,
                desc = 'Next trouble/quickfix item',
            },
            { 'gl', '<cmd>Trouble lsp toggle<CR>', desc = 'LSP information (Trouble)' },
            { 'gL', '<cmd>Trouble loclist toggle<CR>', desc = 'Location list (Trouble)' },
            { 'gQ', '<cmd>Trouble qflist toggle<CR>', desc = 'Quickfix (Trouble)' },
        },
        opts = {
            auto_close = true,
            focus = true,
            icons = {
                indent = {
                    fold_open = ' ',
                    fold_closed = '',
                },
            },
        },
    },
}
