local colors = require('user.themes.' .. vim.g.colors_name .. '.colors')
local mappings = require('cokeline.mappings')

local components = {
    separator = {
        text = function(buffer)
            return buffer.index ~= 1 and '┃' or ''
        end,
        fg = 'TabLineSel',
        truncation = { priority = 2 },
    },
    space = { text = ' ', truncation = { priority = 2 } },
    devicon = {
        text = function(buffer)
            return (mappings.is_picking_focus() or mappings.is_picking_close()) and (buffer.pick_letter .. ' ') or (buffer.devicon.icon .. ' ')
        end,
        fg = function(buffer)
            if mappings.is_picking_close() then
                return buffer.is_focused and colors.yellow or colors.red
            end
            if mappings.is_picking_focus() then
                return buffer.is_focused and colors.yellow or colors.blue
            end
            return buffer.devicon.color
        end,
        style = function()
            return (mappings.is_picking_focus() or mappings.is_picking_close()) and 'bold,italic' or nil
        end,
        truncation = { priority = 3 },
    },
    unique_prefix = {
        text = function(buffer)
            return buffer.unique_prefix
        end,
        fg = 'Comment',
        style = 'italic',
        truncation = { priority = 4, direction = 'left' },
    },
    filename = {
        text = function(buffer)
            return buffer.filename .. ' '
        end,
        style = function(buffer)
            return buffer.is_focused and 'bold' or 'italic'
        end,
        truncation = { priority = 1, direction = 'left' },
    },
    file_readonly = {
        text = function(buffer)
            return (buffer.is_readonly or not vim.api.nvim_get_option_value('modifiable', {buf = buffer.number })) and ' ' or ''
        end,
        fg = colors.red,
        truncation = { priority = 3, direction = 'left' },
    },
    close_or_modified = {
        text = function(buffer)
            return buffer.is_modified and ' ' or ' '
        end,
        fg = function(buffer)
            return buffer.is_modified and colors.green or 'Comment'
        end,
        delete_buffer_on_left_click = true,
        truncation = { priority = 3 },
    },
}

local buffer_width = 28
local get_remaining_space = function(buffer)
    local used_space = 0
    for _, component in pairs(components) do
        used_space = used_space + vim.fn.strwidth(
        (type(component.text) == 'string' and component.text)
        or (type(component.text) == 'function' and component.text(buffer))
        )
    end
    return math.max(0, buffer_width - used_space)
end
local left_padding = {
    text = function(buffer)
        local remaining_space = get_remaining_space(buffer)
        return string.rep(' ', remaining_space / 2 + remaining_space % 2)
    end,
}
local right_padding = {
    text = function(buffer)
        local remaining_space = get_remaining_space(buffer)
        return string.rep(' ', remaining_space / 2)
    end,
}

return {
    default_hl = {
        fg = function(buffer)
            return buffer.is_focused and 'Normal' or 'Comment'
        end,
        bg = 'NONE',
    },
    fill_hl = 'TabLineFill',
    rendering = { max_buffer_width = buffer_width },
    components = {
        components.separator,
        left_padding,
        components.space,
        components.devicon,
        components.unique_prefix,
        components.file_readonly,
        components.filename,
        right_padding,
        components.close_or_modified,
    },
    tabs = {
        placement = 'left',
        components = {
            {
                text = function(tabpage)
                    return string.format("  %s  ", tabpage.number)
                end,
                on_click = function(_, _, _, _, tabpage)
                    tabpage:focus()
                end,
                highlight = function(tabpage)
                    return tabpage.is_active and 'TabLineSel' or 'TabLine'
                end,
                truncation = { priority = 2 },
            },
        },
    },
    rhs = {
        {
            text = '  ' .. vim.fn.fnamemodify(vim.fn.getcwd(), ':t') .. ' ',
            highlight = 'TabLineSel',
            truncation = { priority = 2 },
        },
    },
}
