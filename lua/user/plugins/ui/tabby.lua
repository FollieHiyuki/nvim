local theme = {
    fill = 'TabLineFill',
    head = 'TabLineSel',
    current_tab = 'TabLineSel',
    tab = 'TabLineFill',
}

require('tabby.tabline').set(function(line)
    return {
        {
            { '  ' .. vim.fn.fnamemodify(vim.fn.getcwd(), ':t') .. ' ', hl = theme.head },
            line.sep(' ', theme.head, theme.fill),
        },
        line.tabs().foreach(function(tab)
            local hl = tab.is_current() and theme.current_tab or theme.tab
            return {
                line.sep('', hl, theme.fill),
                tab.is_current() and '󱓻' or '󱓼',
                tab.number(),
                line.sep('', hl, theme.fill),
                hl = hl,
                margin = ' ',
            }
        end),
        line.spacer(),
        line.wins_in_tab(line.api.get_current_tab()).foreach(function(win)
            local name = win.buf_name()
            local icon, color = require('nvim-web-devicons').get_icon_color(name, vim.fn.fnamemodify(name, ':e'), { default = true })
            local hl = win.is_current() and theme.current_tab or theme.tab

            return {
                line.sep('', hl, theme.fill),
                {
                    icon,
                    hl = {
                        fg = color,
                        bg = require('tabby.module.highlight').extract(hl).bg,
                    },
                },
                win.buf_name(),
                win.buf().is_changed() and '' or '',
                line.sep('', hl, theme.fill),
                hl = hl,
                margin = ' ',
            }
        end),
        hl = theme.fill,
    }
end, {
    buf_name = { mode = 'shorten' },
})
