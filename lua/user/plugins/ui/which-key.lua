return {
    plugins = {
        spelling = { enabled = true, suggestions = 30 },
        presets = {
            operators = false,
            motions = false,
            text_objects = false,
            windows = true,
            nav = true,
            z = true,
            g = true,
        },
    },
    -- Exclude mappings without a description
    filter = function(mapping)
        return mapping.desc and mapping.desc ~= ''
    end,
    icons = { rules = false,  separator = '󰜴' },
    win = {
        border = require('user.config.vars').border,
        wo = { winblend = 0 },
    },
    replace = {
        key = {
            { '<space>', 'SPC' },
            { '<cr>', 'RET' },
            { '<tab>', 'TAB' },
        },
    },
    layout = { spacing = 10, align = 'center' },
    spec = {
        {
            mode = { 'n', 'x' },
            { '<leader>d', group = 'Debug' },
            { '<leader>e', group = 'Editor' },
            { '<leader>g', group = 'Git' },
            { '<leader>s', group = 'Search/Replace' },
        },

        -- Register key group names after <leader>
        {
            { '<leader>f', group = 'Finder' },
            { '<leader>fs', group = 'Symbols' },
            { '<leader>l', group = 'LSP' },
            { '<leader>t', group = 'Tasks' },
            { '<leader>p', group = 'Session' },
            -- { '<leader>q', group = 'Quit' },
            { '<leader>u', group = 'UI' },
            { '<leader><Tab>', group = 'Tab' },
        },
    },
}
