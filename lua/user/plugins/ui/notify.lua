local vars = require('user.config.vars')

return {
    level = vars.loglevel,
    stages = 'slide',
    on_open = function(win)
        vim.api.nvim_win_set_config(win, {
            border = vars.border,
            focusable = false,
        })
    end,
    render = 'default',
    timeout = 2000,
    background_colour = 'NormalFloat',
    minimum_width = 40,
    icons = {
        ERROR = vars.icons.notify.error .. ' ',
        WARN = vars.icons.notify.warn .. ' ',
        INFO = vars.icons.notify.info .. ' ',
        DEBUG = vars.icons.notify.debug .. ' ',
        TRACE = vars.icons.notify.trace .. ' ',
    },
    top_down = true,
}
