local vars = require('user.config.vars')

-- There are nvim-cmp's specific icons in the list with no `hl` key so remove them here
local kind = vim.deepcopy(vars.icons.kind)
for key, val in pairs(kind) do
    if val.hl == nil then
        kind[key] = nil
    end
end

local filtered_names = {
    '.bzr',
    '.git',
    '.hg',
    '.pijul',
    '.svn',
    '_dars',
    'node_modules',
    'bazel-out',
    'bazel-bin',
    'bazel-testlogs',
    'plz-out',
    '.plz-cache',
    '.DS_Store',
}

local function getTelescopeOpts(state, path)
    return {
        cwd = path,
        search_dirs = { path },
        attach_mappings = function (prompt_bufnr)
            local actions = require('telescope.actions')
            actions.select_default:replace(function()
                actions.close(prompt_bufnr)
                local selection = require('telescope.actions.state').get_selected_entry()
                local filename = selection.filename
                if filename == nil then
                    filename = selection[1]
                end
                require('neo-tree.sources.filesystem').navigate(state, state.path, filename)
            end)
            return true
        end
    }
end

return {
    auto_clean_after_session_restore = true,
    close_if_last_window = true,
    enable_opened_markers = false,
    open_files_do_not_replace_types = {
        'lazy',
        'aerial',
        'OverseerForm',
        'TelescopePrompt',
        'notify',
        'undotree',
        'Trouble',
        'NvimTree',
        'neo-tree',
        'neo-tree-popup',
        'diff',
        'qf',
        'terminal',
        'help',
    },
    popup_border_style = vars.border,
    source_selector = { winbar = true },
    default_component_configs = {
        diagnostics = {
            symbols = {
                hint = vars.icons.notify.hint,
                info = vars.icons.notify.info,
                warn = vars.icons.notify.warn,
                error = vars.icons.notify.error,
            },
        },
        icon = {
            folder_closed = '󰉋',
            folder_open = '󰝰',
            folder_empty = '',
            folder_empty_open = '',
            default = '',
        },
        modified = { symbol = '●' },
        name = { trailing_slash = true },
        git_status = {
            symbols = {
                added = vars.icons.git.added,
                deleted = vars.icons.git.removed,
                modified = vars.icons.git.modified,
                renamed = '',
                untracked = '',
                ignored = '',
                unstaged = '',
                staged = '',
                conflict = '',
            },
        },
        type = { enabled = false },
        symlink_target = { enabled = true },
    },
    window = {
        width = 35,
        mappings = {
            ['<space>'] = 'none', -- clashed with <leader>, use <Tab> instead
            ['<Tab>'] = 'toggle_node',
        },
    },
    filesystem = {
        window = {
            mappings = {
                O = 'system_open',
                tf = 'telescope_find',
                tg = 'telescope_grep',
            },
        },
        commands = {
            system_open = function(state)
                local path = state.tree:get_node():get_id()
                if vim.fn.has('mac') == 1 then
                    vim.fn.jobstart({ 'open', path }, { detach = true })
                end
                if vim.fn.has('unix') == 1 then
                    vim.fn.jobstart({ 'xdg-open', path }, { detach = true })
                end
            end,
            telescope_find = function(state)
                local path = state.tree:get_node():get_id()
                require('telescope.builtin').find_files(getTelescopeOpts(state, path))
            end,
            telescope_grep = function(state)
                local path = state.tree:get_node():get_id()
                require('telescope.builtin').live_grep(getTelescopeOpts(state, path))
            end,
        },
        check_gitignore_in_search = false,
        filtered_items = {
            visible = true,
            hide_dotfiles = false,
            hide_by_name = filtered_names,
        },
        find_by_full_path_words = true,
        find_args = function(cmd, _, _, args)
            if cmd ~= 'fd' then
                return args
            end

            table.insert(args, '--hidden')

            for _, pattern in ipairs(filtered_names) do
                table.insert(args, '--exclude')
                table.insert(args, pattern)
            end

            return args
        end,
        hijack_netrw_behavior = 'disabled', -- use oil.nvim for this functionality
        use_libuv_file_watcher = true,
    },
    buffers = { terminals_first = true },
    document_symbols = { kinds = kind },
}
