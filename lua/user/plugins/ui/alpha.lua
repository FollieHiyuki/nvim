local header = {
    type = 'text',
    val = {
        [[<-. (`-')_  (`-')  _                 (`-')  _     <-. (`-')  ]],
        [[   \( OO) ) ( OO).-/     .->        _(OO ) (_)       \(OO )_ ]],
        [[,--./ ,--/ (,------.(`-')----. ,--.(_/,-.\ ,-(`-'),--./  ,-.)]],
        [[|   \ |  |  |  .---'( OO).-.  '\   \ / (_/ | ( OO)|   `.'   |]],
        [[|  . '|  |)(|  '--. ( _) | |  | \   /   /  |  |  )|  |'.'|  |]],
        [[|  |\    |  |  .--'  \|  |)|  |_ \     /_)(|  |_/ |  |   |  |]],
        [[|  | \   |  |  `---.  '  '-'  '\-'\   /    |  |'->|  |   |  |]],
        [[`--'  `--'  `------'   `-----'     `-'     `--'   `--'   `--']],
    },
    opts = {
        position = 'center',
        hl = 'DashboardHeader',
    },
}

local footers = {
    [[☆*:.｡. o(≧▽≦)o .｡.:*☆]],
    [[°˖✧ ◝(⁰▿⁰)◜✧˖°]],
    [[✿  ♬ ﾟ+.(｡◡‿◡)♪.+ ﾟ♬ ✿ ]],
    [[— ฅ/ᐠ. ̫ .ᐟ\ฅ —]],
    [[/ᐠ_ ꞈ _ᐟ\ɴʏᴀ~]],
    [[૮₍˶ •. • ⑅₎ა]],
    [[︶꒷꒦︶ ๋࣭ ⭑ ૮₍˶• . • ⑅₎ა]],
    [[｡*ﾟ.*.｡(っ ᐛ )っ 𝖍𝖎]],
    [[ₓ ｡ 𐐪₍ᐢ. ̫ .⑅ᐢ₎𐑂↝]],
    [[ଘ(੭ *ˊᵕˋ)੭ * ੈ♡‧₊˚]],
}

math.randomseed(os.time())
local footer = {
    type = 'text',
    val = footers[math.random(#footers)],
    opts = {
        position = 'center',
        hl = 'DashboardFooter',
    },
}

local button = function(sc, txt, keybind, keybind_opts)
    local sc_ = sc:gsub('%s', ''):gsub('SPC', '<leader>')

    local opts = {
        position = 'center',
        shortcut = sc,
        cursor = 3,
        width = 50,
        align_shortcut = 'right',
        hl = 'DashboardCenter',
        hl_shortcut = 'DashboardShortcut',
    }
    if keybind then
        keybind_opts = vim.F.if_nil(keybind_opts, { noremap = true, silent = true, nowait = true })
        opts.keymap = { 'n', sc_, keybind, keybind_opts }
    end

    local function on_press()
        local key = vim.api.nvim_replace_termcodes(sc_ .. '<Ignore>', true, false, true)
        vim.api.nvim_feedkeys(key, 'normal', false)
    end

    return {
        type = 'button',
        val = txt,
        on_press = on_press,
        opts = opts,
    }
end

local buttons = {
    type = 'group',
    val = {
        button('SPC /', '  Live grep'),
        button('SPC f m', '  Bookmarks'),
        button('SPC f f', '  Find file'),
        button('SPC f r', '  Recent files'),
        button('SPC f p', '  Recent projects'),
        button('SPC p l', '  Load last session'),
        button('SPC SPC', '  Plugin manager'),
    },
    opts = { spacing = 1 },
}

local loaded = {
    type = 'text',
    val = '', -- this is overwritten in the autocmd below
    opts = {
        position = 'center',
        hl = 'DashboardFooter',
    },
}
-- Redraw the dashboard to get Lazy stats and overwrited 'loaded' value
vim.api.nvim_create_autocmd('User', {
    pattern = 'LazyVimStarted',
    callback = function()
        local stats = require('lazy').stats()
        loaded.val = '🎉 Neovim loaded '
        .. stats.count
        .. ' plugins in '
        .. (math.floor(stats.startuptime * 100 + 0.5) / 100)
        .. 'ms'
        pcall(vim.cmd.AlphaRedraw)
    end,
})

return {
    layout = {
        { type = 'padding', val = 7 },
        header,
        { type = 'padding', val = 2 },
        buttons,
        loaded,
        { type = 'padding', val = 1 },
        footer,
        { type = 'padding', val = 7 },
    },
    opts = {
        margin = 5,
        noautocmd = true,
        redraw_on_resize = true,
    },
}
