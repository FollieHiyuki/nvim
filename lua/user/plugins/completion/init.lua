return {
    { 'rcarriga/cmp-dap', ft = { 'dap-repl', 'dapui_watches', 'dapui_hover' } },
    {
        'hrsh7th/nvim-cmp',
        event = 'InsertEnter',
        dependencies = {
            'hrsh7th/cmp-buffer',
            'https://codeberg.org/FelipeLema/cmp-async-path',
            'hrsh7th/cmp-nvim-lsp',
            {
                'garymjr/nvim-snippets',
                dependencies = 'rafamadriz/friendly-snippets',
                config = function()
                    require('snippets').setup {
                        friendly_snippets = true,
                        global_snippets = { 'all', 'global' },
                    }
                end,
            },
        },
        config = function()
            require('user.plugins.completion.cmp')
        end,
    },
}
