local cmp = require('cmp')
local vars = require('user.config.vars')

local sources_config = {
    buffer = { item_menu = 'BUF', option = { keyword_length = 2 } },
    dap = { item_menu = 'DAP' },
    snippets = { item_menu = 'SNIP' },
    nvim_lsp = { item_menu = 'LSP' },
    orgmode = { item_menu = 'ORG' },
    async_path = { item_menu = 'PATH' },
}

local sources = vim.deepcopy(sources_config)
for source, _ in pairs(sources_config) do
    sources[source].name = source
    sources[source].item_menu = nil
end

local has_words_before = function()
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match('%s') == nil
end

local cmpkind_icons = {}
for kind, spec in pairs(vars.icons.kind) do
    cmpkind_icons[kind] = spec.icon
end

cmp.setup {
    view = { entries = { name = 'custom', selection_order = 'near_cursor' } },
    window = {
        documentation = {
            border = vars.border,
            winhighlight = 'NormalFloat:NormalFloat,FloatBorder:FloatBorder',
        },
        completion = {
            scrollbar = true,
            border = vars.border,
            winhighlight = 'NormalFloat:NormalFloat,FloatBorder:FloatBorder',
            completeopt = 'menu,menuone,noinsert',
            keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%(-\w*\)*\)]],
            keyword_length = 1,
        },
    },
    formatting = {
        expandable_indicator = false,
        fields = { 'kind', 'abbr', 'menu' },
        format = function(entry, vim_item)
            vim_item.menu = sources_config[entry.source.name].item_menu

            if vim.tbl_contains({ 'async_path' }, entry.source.name) then
                local icon, hl_group = require('nvim-web-devicons').get_icon(entry:get_completion_item().label)
                if icon then
                    vim_item.kind = icon
                    vim_item.kind_hl_group = hl_group
                    return vim_item
                end
            end

            vim_item.kind = cmpkind_icons[vim_item.kind] .. ' '
            return vim_item
        end,
    },
    mapping = {
        ['<CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        },
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-p>'] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Select },
        ['<C-n>'] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Select },
        ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-c>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.close()
            else
                fallback()
            end
        end),
        -- supertab-like mapping
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item { behavior = cmp.SelectBehavior.Insert }
            elseif vim.snippet.active { direction = 1 } then
                vim.snippet.jump(1)
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end, { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item { behavior = cmp.SelectBehavior.Insert }
            elseif vim.snippet.active { direction = -1 } then
                vim.snippet.jump(-1)
            else
                fallback()
            end
        end, { 'i', 's' }),
    },
    snippet = {
        expand = function(args)
            vim.snippet.expand(args.body)
        end,
    },
    sources = cmp.config.sources {
        sources.nvim_lsp,
        sources.buffer,
        sources.snippets,
        sources.async_path,
    },
}

-- Filetype-based completion sources
cmp.setup.filetype({ 'dap-repl', 'dapui_watches', 'dapui_hover' }, {
    sources = { sources.dap, sources.buffer },
})
cmp.setup.filetype('org', {
    sources = cmp.config.sources {
        sources.orgmode,
        sources.nvim_lsp,
        sources.snippets,
        sources.buffer,
        sources.async_path,
    },
})
