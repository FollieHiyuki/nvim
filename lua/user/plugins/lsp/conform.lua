local vars = require('user.config.vars')

local function get_formatter_path(name)
    local bin_path = vim.fn.exepath(name)
    if bin_path ~= '' then
        return bin_path
    end

    local formatters_path = vim.fn.stdpath('data') .. '/programs'

    if name == 'codespell' then
        return formatters_path .. '/codespell/bin/codespell'
    end

    if name == 'sql-formatter' then
        return formatters_path .. '/sql_formatter/node_modules/.bin/sql-formatter'
    end
end

return {
    log_level = vars.loglevel,
    default_format_opts = {
        lsp_format = 'fallback',
    },
    format_on_save = function()
        if not vars.lsp_format_on_save then
            return
        end
        return { timeout_ms = 500 }
    end,
    formatters_by_ft = {
        sh = { 'shfmt' },
        sql = { 'sql_formatter' },
        lua = { 'stylua' },
        c = { 'clang_format' },
        cpp = { 'clang_format' },
        terraform = { 'tofu_fmt' },
    },
    formatters = {
        codespell = { command = get_formatter_path('codespell') },
        sql_formatter = { command = get_formatter_path('sql-formatter') },
    },
}
