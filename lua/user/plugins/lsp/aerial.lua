local vars = require('user.config.vars')

local icons = {}
for name, val in pairs(vars.icons.kind) do
    if val.hl ~= nil then
        icons[name] = val.icon .. ' '
        vim.api.nvim_set_hl(0, string.format('Aerial%sIcon', name), { link = val.hl })
    end
end
icons['Collapsed'] = '▶'

return {
    backends = { 'lsp', 'treesitter', 'markdown', 'man' },
    layout = { width = 30 },
    show_guides = true,
    highlight_on_hover = true,
    icons = icons,
    filter_kind = false,
    float = { border = vars.border, relative = 'editor' },
    nav = {
        border = vars.border,
        preview = true,
        max_width = 0.8,
        win_opts = { winblend = 0 },
        keymaps = { ['q'] = 'actions.close' },
    },
    lsp = {
        priority = {
            templ = 5, -- put it lower than the default 10
        },
    },
}
