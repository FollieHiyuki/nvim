local util = require('user.util.misc')

local function get_linter_path(name)
    local bin_path = vim.fn.exepath(name)
    if bin_path ~= '' then
        return bin_path
    end

    local linters_path = vim.fn.stdpath('data') .. '/programs'

    if name == 'codespell' then
        return linters_path .. '/codespell/bin/codespell'
    end

    if name == 'cspell' then
        return linters_path .. '/cspell/node_modules/.bin/cspell'
    end

    if name == 'proselint' then
        return linters_path .. '/proselint/bin/proselint'
    end

    if name == 'sqlfluff' then
        return linters_path .. '/sqlfluff/bin/sqlfluff'
    end
end

return {
    -- Use '*' for global linters (enabled on all buffers)
    -- and '_' for fallback linters (enabled when nothing else is)
    linters_by_ft = {
        bzl = { 'buildifier' },
        dockerfile = { 'hadolint' },
        lua = { 'selene' },
        -- org = { 'proselint' },
        -- asciidoc = { 'proselint' },
        sql = { 'sqlfluff' },
        ['*'] = { 'typos', 'cspell' },
    },

    -- `condition` is custom fields.
    linters = {
        codespell = { cmd = get_linter_path('codespell') },
        cspell = {
            cmd = get_linter_path('cspell'),
            condition = util.root_has_file {
                '.cspell.json',
                'cspell.json',
                '.cSpell.json',
                'cSpell.json',
                'cspell.config.json',
            },
        },
        proselint = { cmd = get_linter_path('proselint') },
        selene = { condition = util.root_has_file('selene.toml') },
        sqlfluff = { cmd = get_linter_path('sqlfluff') },
    },
}
