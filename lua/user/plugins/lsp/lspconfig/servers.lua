local lsputil = require('lspconfig.util')
local servers_path = vim.fn.stdpath('data') .. '/programs'

-- Read a file line by line and build a table from them
---@param file string
local function read_dict(file)
    if not file then
        return nil
    end
    local dict = {}
    for line in io.lines(file) do
        table.insert(dict, line)
    end
    return dict
end

local function get_binary_paths(binaries)
    local paths = {}
    for _, bin in ipairs(binaries) do
        local path = vim.fn.exepath(bin)
        if path ~= '' then
            table.insert(paths, path)
        end
    end
    return table.concat(paths, ',')
end

local function deno_root_dir(fname)
    return lsputil.root_pattern('deno.json', 'deno.jsonc', 'deno.lock')(fname)
end

return {
    ansiblels = {
        on_new_config = function(new_config)
            if vim.fn.exepath('ansible-language-server') == '' then
                new_config.cmd = { servers_path .. '/ansiblels/node_modules/.bin/ansible-language-server', '--stdio' }
            end
        end,
        settings = {
            redhat = { telemetry = { enabled = false } },
            ansible = { lightspeed = { enabled = false } },
        },
    },

    basedpyright = {
        on_new_config = function(new_config)
            if vim.fn.exepath('basedpyright-langserver') == '' then
                new_config.cmd = { servers_path .. '/basedpyright/bin/basedpyright-langserver', '--stdio' }
            end
        end,
    },

    bashls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('bash-language-server') == '' then
                new_config.cmd = { servers_path .. '/bashls/node_modules/.bin/bash-language-server', 'start' }
            end
        end,
    },

    -- bazelrc_lsp = {},

    beancount = { init_options = { journalFile = os.getenv('HOME') .. '/Documents/Ledger/ledger.beancount' } },

    biome = {},

    buf_ls = {},

    clangd = {
        filetypes = { 'c', 'cpp', 'objc', 'objcpp', 'cuda' },
        keys = {
            { '<localleader>h', '<cmd>ClangdSwitchSourceHeader<CR>', mode = 'n', desc = 'Switch source header' },
        },
        init_options = {
            clangdFileStatus = true,
        },
        cmd = {
            'clangd',
            '--enable-config',
            '--query-driver=' .. get_binary_paths { 'clang', 'clang++', 'gcc', 'g++' },
            '--background-index',
            '--clang-tidy',
            '--all-scopes-completion',
            '--completion-style=detailed',
            '--pch-storage=memory',
            '--header-insertion=iwyu',
            '--header-insertion-decorators',
            '--import-insertions',
        },
    },

    clojure_lsp = {},

    cssls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('vscode-css-language-server') == '' then
                new_config.cmd = { servers_path .. '/vscode/node_modules/.bin/vscode-css-language-server', '--stdio' }
            end
        end,
    },

    cue = {},

    -- Ref: https://docs.deno.com/runtime/manual/advanced/language_server/overview
    denols = {
        -- Ensure `ts_ls` and `denols` aren't attached to a single buffer at the same time
        root_dir = deno_root_dir,
        init_options = { enable = true, lint = true },
        settings = {
            deno = {
                unstable = true,
                codeLens = {
                    implementations = true,
                    references = true,
                    referencesAllFunctions = true,
                    test = true,
                },
                suggest = {
                    imports = {
                        autoDiscover = false,
                        hosts = {
                            ['https://deno.land'] = false,
                            ['https://nest.land'] = false,
                        },
                    },
                },
            },
        },
    },

    dhall_lsp_server = {},

    dockerls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('docker-langserver') == '' then
                new_config.cmd = { servers_path .. '/dockerls/node_modules/.bin/docker-langserver', '--stdio' }
            end
        end,
    },

    emmet_language_server = {
        on_new_config = function(new_config)
            if vim.fn.exepath('emmet-language-server') == '' then
                new_config.cmd = { servers_path .. '/emmet_language_server/node_modules/.bin/emmet-language-server', '--stdio' }
            end
        end,
    },

    earthlyls = {},

    fennel_ls = {},

    fish_lsp = {
        on_new_config = function(new_config)
            if vim.fn.exepath('fish-lsp') == '' then
                new_config.cmd = { servers_path .. '/fish_lsp/out/cli.js', 'start' }
            end
        end,
    },

    gdscript = {},

    -- gitlab_ci_ls = {
    --     init_options = {
    --         cache_path = vim.fn.stdpath('cache') .. '/gitlab-ci-ls',
    --         log_path = vim.fn.stdpath('log') .. '/gitlab-ci-ls.log',
    --     },
    -- },

    gleam = {},

    -- NOTE: for Bazel setup (GOPACKAGESDRIVER), use neoconf.nvim
    -- Ref: https://github.com/bazelbuild/rules_go/wiki/Editor-setup
    gopls = {
        settings = {
            gopls = {
                gofumpt = true,
                codelenses = {
                    gc_details = true,
                    generate = true,
                    regenerate_cgo = true,
                    run_govulncheck = true,
                    tidy = true,
                    upgrade_dependency = true,
                    vendor = true,
                },
                hints = {
                    assignVariableTypes = true,
                    compositeLiteralFields = true,
                    compositeLiteralTypes = true,
                    constantValues = true,
                    functionTypeParameters = true,
                    parameterNames = true,
                    rangeVariableTypes = true,
                },
                analyses = {
                    fieldalignment = true,
                    nilness = true,
                    shadow = true,
                    unusedparams = true,
                    unusedvariable = true,
                    unusedwrite = true,
                    useany = true,
                },
                usePlaceholders = true,
                staticcheck = true,
                semanticTokens = true,
                noSemanticString = true,
                vulncheck = 'Imports',
                directoryFilters = {
                    '-.git',
                    '-.vscode',
                    '-.vscode-test',
                    '-.idea',
                    '-node_modules',
                    '-plz-out',
                    '-.plz-cache',
                    '-.plz-http-cache',
                    '-bazel-bin',
                    '-bazel-out',
                    '-bazel-testlogs',
                },
            },
        },
    },

    graphql = {
        on_new_config = function(new_config)
            if vim.fn.exepath('graphql-lsp') == '' then
                new_config.cmd = { servers_path .. '/graphql/node_modules/.bin/graphql-lsp', 'server', '-m', 'stream' }
            end
        end,
    },

    guile_ls = {
        filetypes = { 'scheme' },
        root_dir = lsputil.root_pattern('guix.scm', 'manifest.scm', '.guix-channel', '.git'),
    },

    helm_ls = {
        settings = {
            ['helm-ls'] = {
                yamlls = {
                    path = vim.fn.exepath('yaml-language-server') ~= '' and vim.fn.exepath('yaml-language-server') or servers_path .. '/yamlls/node_modules/.bin/yaml-language-server',
                },
            },
        },
    },

    html = {
        on_new_config = function(new_config)
            if vim.fn.exepath('vscode-html-language-server') == '' then
                new_config.cmd = { servers_path .. '/vscode/node_modules/.bin/vscode-html-language-server', '--stdio' }
            end
        end,
    },

    jsonls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('vscode-json-language-server') == '' then
                new_config.cmd = { servers_path .. '/vscode/node_modules/.bin/vscode-json-language-server', '--stdio' }
            end
        end,
        -- Schema catalog: https://www.schemastore.org/api/json/catalog.json
        settings = {
            json = {
                schemas = require('schemastore').json.schemas(),
                validate = { enable = true },
            },
        },
    },

    jsonnet_ls = {},

    koka = {},

    lemminx = {},

    ltex_plus = {
        settings = {
            ltex = {
                language = 'en-US',
                dictionary = {
                    ['en-US'] = read_dict(vim.fn.stdpath('config') .. '/spell/en.utf-8.add'),
                },
                completionEnabled = true,
            },
        },
    },

    lua_ls = {
        settings = {
            Lua = {
                -- codeLens = { enable = true },
                format = { enable = false },
                hint = { enable = true, arrayIndex = 'Disable', setType = true },
                completion = { callSnippet = 'Replace', keywordSnippet = 'Replace' },
                telemetry = { enable = false },
                workspace = { checkThirdParty = false },
            },
        },
    },

    marksman = {},

    mdx_analyzer = {
        on_new_config = function(new_config, new_root_dir)
            if vim.fn.exepath('mdx-language-server') == '' then
                new_config.cmd = { servers_path .. '/mdx_analyzer/node_modules/.bin/mdx-language-server', '--stdio' }
            end

            -- Copied from the default on_new_config settings
            local function get_typescript_server_path(root_dir)
                local project_root = vim.fs.dirname(vim.fs.find('node_modules', { path = root_dir, upward = true })[1])
                return project_root and (project_root .. '/node_modules/typescript/lib') or ''
            end
            if vim.tbl_get(new_config.init_options, 'typescript') and not new_config.init_options.typescript.tsdk then
                new_config.init_options.typescript.tsdk = get_typescript_server_path(new_root_dir)
            end
        end,
        root_dir = lsputil.root_pattern('package.json', 'tsconfig.json'),
    },

    mesonlsp = {},

    nickel_ls = { single_file_support = true },

    nil_ls = {
        root_dir = lsputil.root_pattern('.git', 'flake.lock'),
        settings = {
            ['nil'] = { formatting = { command = { 'nixfmt' } } },
        },
    },

    nushell = {},

    -- TODO: implement handlers for LSP extensions
    -- Ref: https://github.com/ocaml/ocaml-lsp#lsp-extensions
    ocamllsp = {
        root_dir = lsputil.root_pattern('*.opam', '.ocamlformat', 'esy.json', 'dune-project', 'dune-workspace', '.git'),
    },

    -- postgres_lsp = { filetypes = { 'psql' } },

    puppet = {},

    purescriptls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('purescript-language-server') == '' then
                new_config.cmd = { servers_path .. '/purescriptls/node_modules/.bin/purescript-language-server', '--stdio' }
            end
        end,
        -- Ref: https://github.com/nwolverson/vscode-ide-purescript/blob/master/package.json
        settings = {
            purescript = {
                -- addNpmPath = true,
                formatter = 'purs-tidy',
            },
        },
    },

    -- pylsp = {
    --     on_new_config = function(new_config)
    --         if vim.fn.exepath('pylsp') == '' then
    --             new_config.cmd = { servers_path .. '/pylsp/bin/pylsp' }
    --         end
    --     end,
    --     settings = {
    --         pylsp = {
    --             plugins = {
    --                 autopep8 = { enabled = false },
    --                 flake8 = { enabled = true },
    --                 pylint = { enabled = true },
    --                 rope_autoimport = { enabled = true, memory = true },
    --                 rope_completion = { enabled = true },
    --             },
    --         },
    --     },
    -- },

    -- pylyzer = {},

    qmlls = {}, -- from qt6-qtdeclarative-dev package in Alpine

    regal = {},

    remark_ls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('remark-language-server') == '' then
                new_config.cmd = { servers_path .. '/remark_ls/node_modules/.bin/remark-language-server', '--stdio' }
            end
        end,
        single_file_support = false,
    },

    rescriptls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('rescript-language-server') == '' then
                new_config.cmd = { servers_path .. '/rescriptls/server/out/cli.js', '--stdio' }
            end
        end,
    },

    ruff = {},

    -- somesass_ls = {
    --     on_new_config = function(new_config)
    --         if vim.fn.exepath('some-sass-language-server') == '' then
    --             new_config.cmd = { servers_path .. '/somesass_ls/node_modules/.bin/some-sass-language-server', '--stdio' }
    --         end
    --     end,
    -- },

    -- use neoconf on each project to override `cmd` to `sqls -config sqls.yml`.
    sqls = { root_dir = lsputil.root_pattern('sqls.yml') },

    -- starpls = {},

    svelte = {
        on_new_config = function(new_config)
            if vim.fn.exepath('svelteserver') == '' then
                new_config.cmd = { servers_path .. '/svelte/node_modules/.bin/svelteserver', '--stdio' }
            end
        end,
    },

    taplo = {},

    teal_ls = {},

    templ = {},

    texlab = {
        keys = {
            { '<localleader>p', '<cmd>TexlabForward<CR>', mode = 'n', desc = 'Preview LaTEX buffer' },
        },
        settings = {
            texlab = {
                build = { onSave = true },
                forwardSearch = {
                    executable = 'zathura',
                    args = { '--synctex-forward', '%l:1:%f', '%p' },
                },
            },
        },
    },

    -- tflint = { root_dir = lsputil.root_pattern('.tflint.hcl') },

    tilt_ls = {},

    tinymist = {
      settings = {
        tinymist = {
          exportPdf = 'onSave',
          formatterMode = 'typstyle',
        },
      },
    },

    ts_ls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('typescript-language-server') == '' then
                new_config.cmd = { servers_path .. '/ts_ls/node_modules/.bin/typescript-language-server', '--stdio' }
            end
        end,
        root_dir = function(fname)
            if deno_root_dir(fname) == nil then
                return lsputil.root_pattern('package.json', 'tsconfig.json', 'jsconfig.json', '.git')(fname)
            end
        end,
        single_file_support = false,
    },

    volar = {
        on_new_config = function(config, root_dir)
            if vim.fn.exepath('vue-language-server') == '' then
                config.cmd = { servers_path .. '/volar/node_modules/.bin/vue-language-server', '--stdio' }
            end

            local function get_typescript_server_path(dir)
                local global_ts = vim.fn.stdpath('data') .. '/lsp/volar/node_modules/typescript/lib'
                local found_ts = ''

                if
                    lsputil.search_ancestors(dir, function(path)
                        found_ts = lsputil.path.join(path, 'node_modules', 'typescript', 'lib')
                        if lsputil.path.exists(found_ts) then
                            return path
                        end
                    end)
                then
                    return found_ts
                end
                return global_ts
            end

            config.init_options.typescript.tsdk = get_typescript_server_path(root_dir)
        end,
    },

    yamlls = {
        on_new_config = function(new_config)
            if vim.fn.exepath('yaml-language-server') == '' then
                new_config.cmd = { servers_path .. '/yamlls/node_modules/.bin/yaml-language-server', '--stdio' }
            end
        end,
        settings = {
            redhat = { telemetry = { enabled = false } },
            yaml = {
                format = {
                    enable = true,
                    bracketSpacing = true,
                    printWidth = 120,
                    singleQuote = true,
                },
                validate = true,
                schemas = require('schemastore').yaml.schemas(),
                schemaStore = { enable = false, url = '' },
                -- https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#configure-your-ide-to-support-reference-tags
                customTags = { '!reference sequence' },
            },
        },
    },

    zls = {
        settings = {
            warn_style = true,
            highlight_global_var_declarations = true,
        },
    },
}
