local M = {}
local lazy_util = require('lazy.core.util')
local vars = require('user.config.vars')

local function set_virtual_text()
    if vars.lsp_virtual_lines then
        return false
    end
    return {
        prefix = function(diag)
            for type, icon in pairs(vars.icons.notify) do
                if diag.severity == vim.diagnostic.severity[type:upper()] then
                    return icon .. ' '
                end
            end
        end,
        spacing = 2,
        source = 'if_many',
    }
end

function M.configure_diagnostic()
    vim.diagnostic.config {
        virtual_text = set_virtual_text(),
        virtual_lines = vars.lsp_virtual_lines,
        severity_sort = true,
        float = {
            border = vars.border,
            prefix = function(diag)
                local level = vim.diagnostic.severity[diag.severity]
                local prefix = string.format('%s ', vars.icons.notify[level:lower()])
                return prefix, 'Diagnostic' .. level:gsub('^%l', string.upper)
            end,
        },
    }
    -- lsp_lines is annoying in LazyUI, so always disable it there
    vim.diagnostic.config({ virtual_lines = false }, require('lazy.core.config').ns)

    -- :help diagnostic-handlers-example
    -- Display only 1 sign from the highest severity
    local ns = vim.api.nvim_create_namespace('customDiagnosticSign_ns')
    local orig_signs_handler = vim.diagnostic.handlers.signs
    vim.diagnostic.handlers.signs = {
        show = function(_, bufnr, _, opts)
            local diagnostics = vim.diagnostic.get(bufnr)

            local max_severity_per_line = {}
            for _, d in pairs(diagnostics) do
                local m = max_severity_per_line[d.lnum]
                if not m or d.severity < m.severity then
                    max_severity_per_line[d.lnum] = d
                end
            end

            local filtered_diagnostics = vim.tbl_values(max_severity_per_line)
            orig_signs_handler.show(ns, bufnr, filtered_diagnostics, opts)
        end,
        hide = function(_, bufnr)
            orig_signs_handler.hide(ns, bufnr)
        end,
    }

    -- Set diagnostic sign icons
    for _, name in ipairs { 'error', 'warn', 'hint', 'info' } do
        local icon = vars.icons.notify[name]
        local hl = 'DiagnosticSign' .. name:gsub('^%l', string.upper)
        vim.fn.sign_define(hl, { text = icon, texthl = hl })
    end
end

function M.toggle_virtual_lines()
    vars.lsp_virtual_lines = not vars.lsp_virtual_lines

    -- Configure diagnostic again for the new settings
    vim.diagnostic.config {
        virtual_text = set_virtual_text(),
        virtual_lines = vars.lsp_virtual_lines,
    }

    if vars.lsp_virtual_lines then
        lazy_util.info('Virtual lines enabled', { title = 'Diagnostic' })
    else
        lazy_util.info('Virtual lines disabled', { title = 'Diagnostic' })
    end
end

local diagnostic_enabled = true
function M.toggle()
    diagnostic_enabled = not diagnostic_enabled
    if diagnostic_enabled then
        vim.diagnostic.enable()
        lazy_util.info('LSP diagnostic enabled', { title = 'Diagnostic' })
    else
        vim.diagnostic.enable(false)
        lazy_util.warn('LSP diagnostic disabled', { title = 'Diagnostic' })
    end
end

return M
