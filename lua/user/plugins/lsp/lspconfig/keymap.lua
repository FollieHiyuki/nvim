local M = {}

M._keys = nil

function M.get()
    if not M._keys then
        M._keys = {
            { '<leader>uy', function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled {}) end, desc = 'Toggle inlay hint', has = 'inlayHint' },
            { '<leader>la', vim.lsp.buf.code_action, desc = 'Code action', mode = { 'n', 'x' }, has = 'codeAction' },
            { '<leader>ll', vim.lsp.codelens.run, desc = 'Codelens action', has = 'codeLens' },
            { 'gd', '<cmd>Trouble lsp_definitions<CR>', desc = 'Goto definition', has = 'definition' },
            { 'gr', '<cmd>Trouble lsp_references<CR>', desc = 'References' },
            { 'gD', vim.lsp.buf.declaration, desc = 'Goto declaration' },
            { 'gI', '<cmd>Trouble lsp_implementations<CR>', desc = 'Goto implementation' },
            { 'gy', '<cmd>Trouble lsp_type_definitions<CR>', desc = 'Goto type definition' },
            { 'K', vim.lsp.buf.hover, desc = 'Hover' },
            { 'gK', vim.lsp.buf.signature_help, desc = 'Signature help', has = 'signatureHelp' },
            { '<C-k>', vim.lsp.buf.signature_help, mode = 'i', desc = 'Signature help', has = 'signatureHelp' },
        }

        if require('user.util.misc').has('inc-rename.nvim') then
            M._keys[#M._keys + 1] = {
                '<leader>lr',
                function()
                    local inc_rename = require('inc_rename')
                    return ':' .. inc_rename.config.cmd_name .. ' ' .. vim.fn.expand('<cword>')
                end,
                expr = true,
                desc = 'Rename',
                has = 'rename',
            }
        else
            M._keys[#M._keys + 1] = { '<leader>lr', vim.lsp.buf.rename, desc = 'Rename', has = 'rename' }
        end
    end
    return M._keys
end

function M.on_attach(client, buffer)
    local spec = M.get()

    -- Add LSP server specific key bindings
    local servers = require('user.plugins.lsp.lspconfig.servers')
    vim.list_extend(spec, servers[client.name] and servers[client.name].keys or {})

    local Keys = require('lazy.core.handler.keys')
    local keymaps = Keys.resolve(spec)

    for _, keys in pairs(keymaps) do
        local method = keys.has
        if not keys.has or client.supports_method(method:find('/') and method or 'textDocument/' .. method) then
            local opts = Keys.opts(keys)
            opts.has = nil
            opts.silent = opts.silent ~= false
            opts.buffer = buffer
            vim.keymap.set(keys.mode or 'n', keys.lhs, keys.rhs, opts)
        end
    end
end

return M
