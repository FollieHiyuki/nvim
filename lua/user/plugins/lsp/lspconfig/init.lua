local vars = require('user.config.vars')

-- vim.lsp.set_log_level(vars.loglevel)
require('lspconfig.ui.windows').default_options.border = vars.border
require('user.plugins.lsp.lspconfig.diagnostic').configure_diagnostic()

vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(args)
        local client = vim.lsp.get_client_by_id(args.data.client_id)

        -- Key bindings
        require('user.plugins.lsp.lspconfig.keymap').on_attach(client, args.buf)

        -- Bulb for code action hint in statusbar
        if client and client.server_capabilities.codeActionProvider then
            vim.api.nvim_create_autocmd({ 'CursorHold', 'InsertLeave' }, {
                group = vim.api.nvim_create_augroup('LspCodeAction', {}),
                callback = function(event)
                    require('user.util.lsp').codeAction_on_attach(event.buf)
                end,
                buffer = args.buf,
            })
        end

        -- Refresh codelens
        if client and client.server_capabilities.codeLensProvider then
            vim.api.nvim_create_autocmd({ 'InsertLeave', 'BufEnter', 'CursorHold' }, {
                group = vim.api.nvim_create_augroup('LspCodeLens', {}),
                callback = function(event)
                    vim.lsp.codelens.refresh { bufnr = event.buf }
                end,
                buffer = args.buf,
            })
        end

        -- Document highlight
        if client and client.server_capabilities.documentHighlightProvider then
            local augroup = vim.api.nvim_create_augroup('LspDocumentHighlight', { clear = false })

            vim.api.nvim_create_autocmd({ 'CursorHold', 'CursorHoldI' }, {
                group = augroup,
                callback = vim.lsp.buf.document_highlight,
                buffer = args.buf,
            })
            vim.api.nvim_create_autocmd({ 'CursorMoved', 'CursorMovedI' }, {
                group = augroup,
                callback = vim.lsp.buf.clear_references,
                buffer = args.buf,
            })
            vim.api.nvim_create_autocmd('LspDetach', {
                group = vim.api.nvim_create_augroup('LspDocumentHighlightDetach', {}),
                callback = function(event)
                    vim.lsp.buf.clear_references()
                    vim.api.nvim_clear_autocmds { group = 'LspDocumentHighlight', buffer = event.buf }
                end,
            })
        end

        -- Prefer hover capability from other LSP servers (pylsp/basedpyright/pylyzer)
        if client and client.name == 'ruff' then
            client.server_capabilities.hoverProvider = false
        end
    end,
})

local servers = require('user.plugins.lsp.lspconfig.servers')
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
for server, server_opts in pairs(servers) do
    local opts = vim.tbl_deep_extend('force', {
        capabilities = vim.deepcopy(capabilities),
    }, server_opts or {})

    require('lspconfig')[server].setup(opts)
end
