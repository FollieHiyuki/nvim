local lazy_util = require('lazy.core.util')

return {
    'b0o/SchemaStore.nvim',
    {
        'neovim/nvim-lspconfig',
        event = { 'BufReadPost', 'BufNewFile' },
        dependencies = {
            { 'folke/neoconf.nvim', cmd = 'Neoconf', config = true },
            { 'https://git.sr.ht/~whynothugo/lsp_lines.nvim', config = true },
        },
        config = function()
            require('user.plugins.lsp.lspconfig')
        end,
    },
    {
        'mfussenegger/nvim-lint',
        event = { 'BufReadPost', 'BufNewFile', 'BufWritePre' },
        opts = function()
            return require('user.plugins.lsp.lint')
        end,
        config = function(_, opts)
            local lint = require('lint')

            for name, linter in pairs(opts.linters) do
                if type(linter) == 'table' and type(lint.linters[name]) == 'table' then
                    lint.linters[name] = vim.tbl_deep_extend('force', lint.linters[name], linter)
                else
                    lint.linters[name] = linter
                end
            end
            lint.linters_by_ft = opts.linters_by_ft

            local function debounce(ms, fn)
                local timer = vim.uv.new_timer()
                return function(...)
                    local argv = { ... }
                    timer:start(ms, 0, function()
                        timer:stop()
                        vim.schedule_wrap(fn)(unpack(argv))
                    end)
                end
            end

            vim.api.nvim_create_autocmd({ 'BufWritePost', 'BufReadPost', 'InsertLeave' }, {
                group = vim.api.nvim_create_augroup('nvim-lint', {}),
                callback = debounce(100, function()
                    local linters = require('user.util.lsp').active_linters(0)
                    if #linters > 0 then
                        lint.try_lint(linters)
                    end
                end),
            })

            -- Like :ConformInfo, but more basic
            vim.api.nvim_create_user_command('LinterInfo', function()
                local linters = require('user.util.lsp').active_linters(0)
                if #linters > 0 then
                    lazy_util.info('Linters for this buffer:\n' .. table.concat(linters, '\n'), { title = 'nvim-lint' })
                else
                    lazy_util.warn('No linters available for this buffer', { title = 'nvim-lint' })
                end
            end, {
                desc = 'Print the list of active linters',
            })
        end,
    },
    {
        'stevearc/conform.nvim',
        event = 'BufWritePre',
        cmd = 'ConformInfo',
        keys = {
            {
                '<leader>lF',
                function()
                    require('conform').format { formatters = { 'injected' } }
                end,
                mode = { 'n', 'x' },
                desc = 'Format injected codeblock',
            },
            {
                '<leader>lf',
                function()
                    require('conform').format()
                end,
                mode = { 'n', 'x' },
                desc = 'Format buffer',
            },
            {
                '<leader>uf',
                function()
                    local vars = require('user.config.vars')

                    vars.lsp_format_on_save = not vars.lsp_format_on_save
                    if vars.lsp_format_on_save then
                        lazy_util.info('Enabled format on save', { title = 'Format' })
                    else
                        lazy_util.warn('Disabled format on save', { title = 'Format' })
                    end
                end,
                mode = 'n',
                desc = 'Toggle format on save',
            },
        },
        opts = function()
            return require('user.plugins.lsp.conform')
        end,
        init = function()
            vim.o.formatexpr = 'v:lua.require"conform".formatexpr()'
        end,
        config = function(_, opts)
            require('conform').setup(opts)
        end,
    },
    { 'smjonas/inc-rename.nvim', cmd = 'IncRename', opts = { hl_group = 'IncSearch' } },
    {
        'stevearc/aerial.nvim',
        keys = {
            { '<leader>lg', '<cmd>AerialToggle<CR>', desc = 'Symbols' },
            { '<leader>lG', '<cmd>AerialNavToggle<CR>', desc = 'Symbols (Nav)' },
        },
        cmd = { 'AerialToggle', 'AerialNavToggle' },
        opts = function()
            return require('user.plugins.lsp.aerial')
        end,
    },
}
