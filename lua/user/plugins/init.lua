return {
    -- lazy.nvim can manage itself
    { 'folke/lazy.nvim', version = '*' },

    -- dependencies needed by the other
    'MunifTanjim/nui.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons',
    { 'tpope/vim-repeat', event = { 'BufNewFile', 'BufReadPost' } },
}
