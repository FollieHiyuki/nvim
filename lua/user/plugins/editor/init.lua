local ts_parsers_path = require('user.config.vars').ts_parsers_path

return {
    { 'MTDL9/vim-log-highlighting', ft = 'log' },
    {
        'nvim-orgmode/orgmode',
        ft = 'org',
        config = function()
            require('user.plugins.editor.orgmode')
        end,
    },
    {
        'lukas-reineke/headlines.nvim',
        ft = { 'org', 'markdown' },
        opts = function()
            return require('user.plugins.editor.headlines')
        end,
        config = function(_, opts)
            -- NOTE: lazily starting up headlines.nvim to prevent it from slowing down on file opening
            vim.schedule(function()
                require('headlines').setup(opts)
                require('headlines').refresh()
            end)
        end,
    },
    {
        'folke/ts-comments.nvim',
        event = 'VeryLazy',
        config = true,
    },
    {
        'folke/todo-comments.nvim',
        cmd = { 'TodoTrouble', 'TodoTelescope' },
        event = { 'BufReadPost', 'BufNewFile' },
        keys = {
            { ']t', function() require('todo-comments').jump_next() end, desc = 'Next todo comment' },
            { '[t', function() require('todo-comments').jump_prev() end, desc = 'Previous todo comment' },
            { 'gt', '<cmd>TodoTrouble<CR>', desc = 'Todo items' },
            { '<leader>ft', '<cmd>TodoTelescope<CR>', desc = 'Todo items' },
        },
        opts = require('user.plugins.editor.todo-comments'),
    },
    {
        'nvim-treesitter/nvim-treesitter',
        build = { 'mkdir -p ' .. ts_parsers_path, ':TSUpdate' },
        event = { 'BufReadPost', 'BufNewFile' },
        keys = {
            { '<C-Space>', desc = 'Increment selection' },
            { '<BS>', desc = 'Decrement selection', mode = 'x' },
        },
        dependencies = {
            'hiphish/rainbow-delimiters.nvim',
            { 'LiadOz/nvim-dap-repl-highlights', config = true },
            {
                'nvim-treesitter/nvim-treesitter-context',
                keys = {
                    { '[c', function() require('treesitter-context').go_to_context() end, desc = 'Go to context' },
                },
            },
            {
                'andymass/vim-matchup',
                init = function()
                    vim.g.matchup_matchparen_offscreen = { method = 'popup' }
                end,
            },
        },
        init = function(plugin)
            vim.opt.rtp:prepend(ts_parsers_path)

            -- Make queries available early, since lots of plugins don't require nvim-treesitter anymore
            require('lazy.core.loader').add_to_rtp(plugin)
            require('nvim-treesitter.query_predicates')
        end,
        opts = require('user.plugins.editor.treesitter'),
        config = function(_, opts)
            -- Force the usage of gcc instead of clang
            -- Ref: https://github.com/nvim-treesitter/nvim-treesitter/issues/1449
            require('nvim-treesitter.install').compilers = { 'gcc' }

            require('nvim-treesitter.configs').setup(opts)
        end,
    },
    {
        'windwp/nvim-ts-autotag',
        ft = {
            'html',
            'javascript',
            'typescript',
            'javascriptreact',
            'typescriptreact',
            'vue',
            'xml',
            'php',
            'markdown',
            'svelte',
            'rescript',
            'glimmer',
            'astro',
            'handlebars',
            'templ',
        },
        opts = {
            filetypes = {
                'html',
                'javascript',
                'javascriptreact',
                'typescript',
                'typescriptreact',
                'svelte',
                'vue',
            },
        },
    },
    {
        'lukas-reineke/indent-blankline.nvim',
        event = { 'BufReadPost', 'BufNewFile' },
        main = 'ibl',
        opts = require('user.plugins.editor.indent-blankline'),
    },
    {
        'mizlan/iswap.nvim',
        cmd = { 'ISwapWith', 'ISwap' },
        keys = {
            { '<leader>es', '<cmd>ISwapWith<CR>', desc = 'Swap arguments' },
        },
        opts = { hl_grey = 'LineNr', autoswap = true },
    },
    {
        'danymat/neogen',
        cmd = 'Neogen',
        keys = {
            { '<leader>eg', function() require('neogen').generate() end, desc = 'Generate annotation' },
            { '<C-l>', function() require('neogen').jump_next() end, desc = 'Next annotation', mode = 'i' },
            { '<C-h>', function() require('neogen').jump_prev() end, desc = 'Previous annotation', mode = 'i' },
        },
        opts = {
            enabled = true,
            input_after_comment = true,
        },
    },
    {
        'folke/zen-mode.nvim',
        cmd = 'ZenMode',
        keys = {
            { '<leader>ez', '<cmd>ZenMode', desc = 'Zen mode' },
        },
        dependencies = {
            {
                'folke/twilight.nvim',
                cmd = { 'Twilight', 'TwilightEnable' },
                opts = require('user.plugins.editor.twilight'),
            },
        },
        opts = { window = { width = 150 } },
    },
    {
        'monaqa/dial.nvim',
        keys = {
            { '<C-a>', function() return require('dial.map').inc_normal() end, expr = true, desc = 'Increment' },
            { '<C-x>', function() return require('dial.map').dec_normal() end, expr = true, desc = 'Decrement' },
            { '<C-a>', function() return require('dial.map').inc_visual() end, expr = true, desc = 'Increment', mode = 'x' },
            { '<C-x>', function() return require('dial.map').dec_visual() end, expr = true, desc = 'Decrement', mode = 'x' },
            { 'g<C-a>', function() return require('dial.map').inc_gvisual() end, expr = true, desc = 'Increment (multiline)', mode = 'x' },
            { 'g<C-x>', function() return require('dial.map').dec_gvisual() end, expr = true, desc = 'Decrement (multiline)', mode = 'x' },
        },
        config = function()
            local augend = require('dial.augend')
            require('dial.config').augends:register_group {
                default = {
                    augend.integer.alias.decimal_int,
                    augend.integer.alias.hex,
                    augend.constant.alias.bool,
                    augend.constant.new { elements = { 'True', 'False' }, word = true, cyclic = true },
                    augend.hexcolor.new { case = 'lower' },
                    augend.semver.alias.semver,
                    augend.date.alias['%Y/%m/%d'],
                },
            }
        end,
    },
    {
        'max397574/better-escape.nvim',
        event = 'InsertCharPre',
        opts = true,
    },
    {
        'eraserhd/parinfer-rust',
        build = 'cargo build --locked --release',
        ft = {
            'clojure',
            'lisp',
            'scheme',
            'fennel',
            'racket',
            'hy',
            'janet',
            'carp',
            'wast',
        },
    },
    {
        'jbyuki/venn.nvim',
        cmd = 'VBox',
        keys = {
            {
                '<leader>ev', function()
                    local venn_enabled = vim.inspect(vim.b.venn_enabled)
                    local key_opts = { noremap = true, silent = true, buffer = true }
                    local lazy_util = require('lazy.core.util')

                    if venn_enabled == 'nil' then
                        vim.b.venn_enabled = true
                        vim.opt_local.virtualedit = 'all'

                        -- Draw lines with WASD keystroke
                        vim.keymap.set('n', 'A', '<C-v>h:VBox<CR>', key_opts)
                        vim.keymap.set('n', 'A', '<C-v>h:VBox<CR>', key_opts)
                        vim.keymap.set('n', 'S', '<C-v>j:VBox<CR>', key_opts)
                        vim.keymap.set('n', 'W', '<C-v>k:VBox<CR>', key_opts)
                        vim.keymap.set('n', 'D', '<C-v>l:VBox<CR>', key_opts)
                        -- Draw boxes by pressing 'f' with visual selection
                        vim.keymap.set('x', 'f', ':VBox<CR>', key_opts)

                        lazy_util.info('Virtual box editing mode enabled.', { title = 'Venv mode', icon = ' ' })
                    else
                        vim.b.venn_enabled = nil
                        vim.opt_local.virtualedit = vim.opt.virtualedit:get()

                        vim.keymap.del('n', 'A', { buffer = true })
                        vim.keymap.del('n', 'S', { buffer = true })
                        vim.keymap.del('n', 'W', { buffer = true })
                        vim.keymap.del('n', 'D', { buffer = true })
                        vim.keymap.del('x', 'f', { buffer = true })

                        lazy_util.info('Virtual box editing mode disabled.', { title = 'Venv mode', icon = ' ' })
                    end
                end,
                desc = 'Virtual box edit',
            },
        },
    },
    {
        'folke/flash.nvim',
        keys = {
            { '-', function() require('flash').jump() end, mode = { 'n', 'x', 'o' }, desc = 'Flash' },
            { '_', function() require('flash').treesitter() end, mode = { 'n', 'x', 'o' }, desc = 'Flash (Treesitter)' },
            { 'gs', function() require('flash').remote() end, mode = 'o', desc = 'Flash (remote)' },
            { 'gS', function() require('flash').treesitter_search() end, mode = { 'x', 'o' }, desc = 'Flash search (Treesitter)' },
            { '<C-s>', function() require('flash').toggle() end, mode = 'c', desc = 'Toggle Flash search' },
            'f', 'F', 't', 'T',
        },
        opts = {
            search = { incremental = true },
            label = { after = false, before = true },
            modes = {
                search = { enabled = false },
                char = { jump_labels = true, multi_line = false },
            },
            prompt = { enabled = false },
        },
    },
    {
        'Wansmer/treesj',
        keys = {
            { 'gJ', '<cmd>TSJToggle<CR>', desc = 'Join toggle' },
        },
        opts = { use_default_keymaps = false, max_join_length = 150 },
    },
    {
        'echasnovski/mini.move',
        keys = {
            { '<A-h>', mode = { 'n', 'x' }, desc = 'Move left' },
            { '<A-j>', mode = { 'n', 'x' }, desc = 'Move down' },
            { '<A-k>', mode = { 'n', 'x' }, desc = 'Move up' },
            { '<A-l>', mode = { 'n', 'x' }, desc = 'Move right' },
        },
        opts = {
            options = {
                reindent_linewise = true,
            },
        },
        config = function(_, opts)
            require('mini.move').setup(opts)
        end,
    },
    {
        'echasnovski/mini.ai',
        event = 'VeryLazy',
        dependencies = {
            {
                'nvim-treesitter/nvim-treesitter-textobjects',
                -- we only need the queries, other functions aren't used
                init = function()
                    require('lazy.core.loader').disable_rtp_plugin('nvim-treesitter-textobjects')
                end,
            },
        },
        config = function()
            require('user.plugins.editor.mini-ai')
        end,
    },
    {
        'echasnovski/mini.pairs',
        event = 'InsertEnter',
        config = function()
            require('mini.pairs').setup()
        end,
    },
    {
        'echasnovski/mini.align',
        keys = function(_, keys)
            local plugin = require('lazy.core.config').spec.plugins['mini.align']
            local opts = require('lazy.core.plugin').values(plugin, 'opts', false)
            local mappings = {
                { opts.mappings.start, mode = { 'n', 'v' }, desc = 'Align text' },
                { opts.mappings.start_with_preview, mode = { 'n', 'v' }, desc = 'Align text (with preview)' },
            }
            mappings = vim.tbl_filter(function(m)
                return m[1] and #m[1] > 0
            end, mappings)
            return vim.list_extend(mappings, keys)
        end,
        opts = { mappings = { start = 'ga', start_with_preview = 'gA' } },
        config = function(_, opts)
            require('mini.align').setup(opts)
        end,
    },
    {
        'echasnovski/mini.comment',
        keys = {
            { 'gc', mode = 'o' },
            { 'gc', mode = { 'n', 'x' }, desc = 'Toggle comment' },
            { 'gcc', mode = 'n', desc = 'Toggle comment on current line' },
        },
        config = true,
    },
    {
        'echasnovski/mini.surround',
        keys = function(_, keys)
            local plugin = require('lazy.core.config').spec.plugins['mini.surround']
            local opts = require('lazy.core.plugin').values(plugin, 'opts', false)
            local mappings = {
                { opts.mappings.add, mode = { 'n', 'x' }, desc = 'Add surrounding' },
                { opts.mappings.delete, desc = 'Delete surrounding' },
                { opts.mappings.find, desc = 'Find surrounding' },
                { opts.mappings.find_left, desc = 'Find left surrounding' },
                { opts.mappings.highlight, desc = 'Highlight surrounding' },
                { opts.mappings.replace, desc = 'Replace surrounding' },
                { opts.mappings.update_n_lines, desc = 'Update `MiniSurround.config.n_lines`' },
            }
            mappings = vim.tbl_filter(function(m)
                return m[1] and #m[1] > 0
            end, mappings)
            return vim.list_extend(mappings, keys)
        end,
        opts = {
            mappings = {
                add = 'sa',
                delete = 'sd',
                find = 'sf',
                find_left = 'sF',
                highlight = 'sh',
                replace = 'sr',
                update_n_lines = 'sn',
            },
            silent = true,
            search_method = 'cover_or_next',
            highlight_duration = vim.opt.timeoutlen:get(),
        },
        config = function(_, opts)
            require('mini.surround').setup(opts)
        end,
    },
    {
        'echasnovski/mini.animate',
        event = 'VeryLazy',
        opts = function()
            return require('user.plugins.editor.mini-animate')
        end,
        config = function(_, opts)
            require('mini.animate').setup(opts)
        end,
    },
}
