return {
    indent = { char = '│', tab_char = '│' },
    scope = {
        show_start = false,
        show_end = false,
        include = { node_type = { ['*'] = { '*' } } },
    },
    exclude = {
        filetypes = {
            'alpha',
            'lazy',
            'log',
            'notify',
            'undotree',
            'NvimTree',
            'neo-tree',
            'neo-tree-popup',
            'diff',
            'qf',
            'help',
            'prompt',
            'noice',
            'aerial',
            'TelescopePrompt',
            'Trouble',
            'OverseerForm',
            'gitcommit',
            'markdown',
            'org',
            'json',
            'txt',
            '', -- for all buffers without a filetype
        },
    },
}
