vim.treesitter.language.register('starlark', 'tiltfile')

return {
    parser_install_dir = require('user.config.vars').ts_parsers_path,
    ensure_installed = 'all',
    ignore_install = { 'org' },
    sync_install = false,
    auto_install = false,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = { 'org' },
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = '<C-Space>',
            node_incremental = '<C-Space>',
            scope_incremental = '<nop>',
            node_decremental = '<BS>',
        },
    },
    indent = { enable = true },
    -- TODO: update the settings when https://github.com/andymass/vim-matchup/pull/330 is merged
    matchup = {
        enable = true,
        disable_virtual_text = true,
        include_match_words = true,
    },
}
