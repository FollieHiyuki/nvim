local c = require('user.themes.' .. vim.g.colors_name .. '.colors')

local keyword_faces = {
    TODO = ':foreground ' .. c.green .. ' :weight bold',
    WAIT = ':foreground ' .. c.yellow .. ' :weight bold',
    HOLD = ':foreground ' .. c.yellow .. ' :weight bold',
    IDEA = ':foreground ' .. c.cyan .. ' :weight bold :slant italic',
    DONE = ':foreground ' .. c.grey_bright .. ' :weight bold',
    OKAY = ':foreground ' .. c.purple .. ' :weight bold :slant italic',
    KILL = ':foreground ' .. c.red .. ' :weight bold',
    CANCELED = ':foreground ' .. c.red .. ' :weight bold',
    YES = ':foreground ' .. c.green .. ' :weight bold :underline on',
    NO = ':foreground ' .. c.red .. ' :weight bold :underline on',
}

local keywords = {}
for k, _ in pairs(keyword_faces) do
    table.insert(keywords, k)
end

require('orgmode').setup {
    -- General settings
    org_agenda_files = { '~/Documents/Org/agenda/*' },
    org_default_notes_file = '~/Documents/Org/notes.org',
    org_todo_keywords = keywords,
    org_todo_keyword_faces = keyword_faces,
    org_hide_emphasis_markers = true,
    org_highlight_latex_and_related = 'entities',

    -- Agenda settings
    org_deadline_warning_days = 7,
    org_agenda_span = 'week',
    org_agenda_start_on_weekday = 7, -- Start a week in Sunday
    org_agenda_min_height = 15,

    -- Tags settings
    org_use_tag_inheritance = false,

    -- Other stuff
    win_border = require('user.config.vars').border,

    -- Notifications
    notifications = {
        enabled = true,
        cron_enabled = false,
        repeater_reminder_time = false,
        deadline_warning_reminder_time = false,
        reminder_time = { 0, 5, 10 },
        deadline_reminder = true,
        scheduled_reminder = true,
        notifier = function(tasks)
            for _, task in ipairs(tasks) do
                local task_type = string.format('%s: <%s>', task.type, task.time:to_string())
                local task_description = string.format('%s %s %s', string.rep('*', task.level), task.todo, task.title)
                require('lazy.core.util').info(string.format('%s\n%s', task_description, task_type), {
                    title = string.format('%s (%s)', task.category, task.humanized_duration),
                    icon = '󱣹 ',
                    on_open = function(win)
                        local buf = vim.api.nvim_win_get_buf(win)
                        vim.api.nvim_set_option_value('filetype', 'org', { buf = buf })
                    end,
                })
            end
        end,
    },
}

require('which-key').add {
    { '<leader>o', group = 'Org' },
    { '<leader>oi', group = 'Insert' },
    { '<leader>ox', group = 'Clock' },
    { '<leader>ol', group = 'Link' },
    { '<leader>ob', group = 'Babel' },
    { '<leader>od', group = 'Date' },
    { '<leader>on', group = 'Note' },
}
