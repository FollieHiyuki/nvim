-- Don't use animate when scrolling with the mouse
local mouse_scrolled = false
for _, scroll in ipairs({ 'Up', 'Down' }) do
    local key = '<ScrollWheel' .. scroll .. '>'
    vim.keymap.set({ 'n', 'i' }, key, function()
        mouse_scrolled = true
        return key
    end, { expr = true })
end

local updatetime = vim.opt.updatetime:get()
local animate = require('mini.animate')
return {
    cursor = {
        path = animate.gen_path.line(),
        timing = animate.gen_timing.linear { duration = updatetime, unit = 'total' },
    },
    resize = { timing = animate.gen_timing.linear { duration = updatetime, unit = 'total' }, },
    scroll = {
        timing = animate.gen_timing.linear { duration = updatetime, unit = 'total' },
        subscroll = animate.gen_subscroll.equal {
            predicate = function(total_scroll)
                if mouse_scrolled then
                    mouse_scrolled = false
                    return false
                end
                return total_scroll > 1
            end,
        },
    },
    open = { enable = false },
    close = { enable = false },
}
