local border = require('user.config.vars').border

local title_border
if border == 'single' or border == 'rounded' then
    title_border = { '┤ ', ' ├' }
else
    title_border = { '╣ ', ' ╠' }
end

return {
    default_engine = 'google',
    command = { default_mode = 'interactive' },
    ui = {
        width_percentage = 0.8,
        height_percentage = 0.6,
    },
    help = { separator = '  ' },
    select = {
        prompt_prefix = ' ',
        selection_caret = ' ',
    },
    window = {
        title_border = title_border,
        window_config = { border = border },
        options = {
            winhighlight = 'Normal:NormalFloat,SignColumn:NormalFloat,FloatBorder:FloatBorder',
        },
    },
    controls = { updatetime = vim.opt.updatetime:get() },
}
