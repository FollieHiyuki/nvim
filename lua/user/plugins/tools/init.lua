local vars = require('user.config.vars')

return {
    {
        'echasnovski/mini.bufremove',
        opts = { silent = true },
        config = function(_, opts)
            require('mini.bufremove').setup(opts)
        end,
    },
    {
        'akinsho/toggleterm.nvim',
        cmd = 'ToggleTerm',
        keys = {
            { [[<C-\>]], '<cmd>ToggleTerm<CR>', desc = 'Terminal' },
            { [[<C-S-\>]], '<cmd>ToggleTerm direction=float<CR>', desc = 'Float terminal' },
        },
        opts = {
            shade_terminals = false,
            float_opts = {
                border = vars.border,
                winblend = 0,
            },
            winbar = { enabled = true },
        },
    },
    {
        'nvim-telescope/telescope.nvim',
        cmd = 'Telescope',
        keys = {
            -- File pickers
            { '<leader>/', '<cmd>Telescope live_grep<CR>', desc = 'Live grep' },
            { '<leader>fe', '<cmd>Telescope file_browser<CR>', desc = 'File browser' },
            { '<leader>ff', '<cmd>Telescope find_files<CR>', desc = 'Files' },
            -- Vim pickers
            { '<leader>:', '<cmd>Telescope command_history<CR>', desc = 'Command history' },
            { '<leader>fa', '<cmd>Telescope autocommands<CR>', desc = 'Auto commands' },
            { '<leader>fb', '<cmd>Telescope buffers sort_mru=true sort_lastused=true<CR>', desc = 'Buffers' },
            { '<leader>fh', '<cmd>Telescope help_tags<CR>', desc = 'Help pages' },
            { '<leader>fk', '<cmd>Telescope keymaps<CR>', desc = 'Keymaps (normal mode)' },
            { '<leader>fm', '<cmd>Telescope marks<CR>', desc = 'Marks' },
            { '<leader>fo', '<cmd>Telescope vim_options<CR>', desc = 'Vim options' },
            { '<leader>fr', '<cmd>Telescope oldfiles<CR>', desc = 'Recent files' },
            { '<leader>fz', '<cmd>Telescope current_buffer_fuzzy_find<CR>', desc = 'Current buffer' },
            -- Git pickers
            { '<leader>ga', '<cmd>Telescope git_stash<CR>', desc = 'Stash items' },
            { '<leader>gm', '<cmd>Telescope git_branches<CR>', desc = 'Branches' },
            { '<leader>gc', '<cmd>Telescope git_bcommits<CR>', desc = 'Buffer commits' },
            { '<leader>gC', '<cmd>Telescope git_commits<CR>', desc = 'Commits' },
        },
        dependencies = {
            'nvim-telescope/telescope-file-browser.nvim',
            { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
        },
        config = function()
            require('user.plugins.tools.telescope')
        end,
    },
    {
        'ziontee113/icon-picker.nvim',
        cmd = { 'IconPickerNormal', 'IconPickerInsert', 'IconPickerYank' },
        keys = {
            { '<leader>fs', '<cmd>IconPickerNormal<CR>', desc = 'Pick icons' },
        },
        opts = { disable_legacy_commands = true },
    },
    {
        'LukasPietzschmann/telescope-tabs',
        keys = {
            { '<leader>f<Tab>', function() require('telescope-tabs').list_tabs() end, desc = 'Tabs' },
        },
        opts = {
            entry_formatter = function(tab_id, _, file_names, _, is_current)
                local current_icon = is_current and ' ' or '  '
                if require('user.util.misc').has('tabby.nvim') then
                    local tab_name = require('tabby.feature.tab_name').get(tab_id)
                    return string.format('%d: %s%s', tab_id, current_icon, tab_name)
                end
                return string.format('%d: %s%s', tab_id, current_icon, table.concat(file_names, ', '))
            end,
            entry_ordinal = function(tab_id, _, file_names)
                if require('user.util.misc').has('tabby.nvim') then
                    return require('tabby.feature.tab_name').get(tab_id)
                end
                return table.concat(file_names, ' ')
            end,
        },
    },
    {
        'ahmedkhalf/project.nvim',
        event = { 'BufReadPost', 'BufNewFile' },
        keys = {
            {
                '<leader>fp', function()
                    local telescope = require('telescope')
                    telescope.load_extension('projects')
                    telescope.extensions.projects.projects()
                end,
                desc = 'Recent projects',
            },
        },
        config = function()
            require('user.plugins.tools.project')
        end,
    },
    {
        'stevearc/overseer.nvim',
        cmd = {
            'OverseerToggle',
            'OverseerBuild',
            'OverseerInfo',
            'OverseerRunCmd',
            'OverseerRun',
        },
        keys = {
            { '<leader>tb', '<cmd>OverseerBuild<CR>', desc = 'Build task' },
            { '<leader>ti', '<cmd>OverseerInfo<CR>', desc = 'Info' },
            { '<leader>tl', '<cmd>OverseerToggle<CR>', desc = 'Task list' },
            { '<leader>tr', '<cmd>OverseerRun<CR>', desc = 'Run task' },
        },
        opts = function()
            return require('user.plugins.tools.overseer')
        end,
    },
    {
        'stevearc/stickybuf.nvim',
        event = 'BufReadPost',
        cmd = { 'PinBuffer', 'PinBuftype', 'Unpin', 'PinFileType' },
        opts = { get_auto_pin = require('user.plugins.tools.stickybuf') },
    },
    {
        'stevearc/oil.nvim',
        event = 'UIEnter',
        keys = {
            {
                '<leader>o',
                function()
                    vim.ui.input({
                        prompt = 'Path: ',
                        default = vim.fn.getcwd(),
                        completion = 'file',
                    }, function(path)
                        if path then
                            require('oil').open_float(path)
                        end
                    end)
                end,
                desc = 'Open directory'
            },
        },
        opts = function()
            return require('user.plugins.tools.oil')
        end,
    },
    {
        'willothy/flatten.nvim',
        lazy = false,
        priority = 1001,
        opts = {
            window = { open = 'alternate' },
            hooks = {
                should_block = function(argv)
                    return vim.tbl_contains(argv, '-b') or vim.tbl_contains(argv, '-d')
                end,
            },
        },
    },
    {
        'NeogitOrg/neogit',
        cmd = 'Neogit',
        keys = {
            {'<leader>go', '<cmd>Neogit<CR>', desc = 'Neogit' },
        },
        dependencies = {
            {
                'sindrets/diffview.nvim',
                cmd = { 'DiffviewOpen', 'DiffviewFileHistory' },
            },
        },
        opts = {
            signs = {
                section = { '󰄾', '󰄼' },
                item = { '', '' },
            },
            disable_context_highlighting = true,
            integrations = { diffview = true, telescope = true },
            telescope_sorter = function()
                return require("telescope").extensions.fzf.native_fzf_sorter()
            end,
        },
    },
    {
        'lewis6991/gitsigns.nvim',
        event = { 'BufNewFile', 'BufReadPost' },
        dependencies = 'plenary.nvim',
        opts = require('user.plugins.tools.gitsigns'),
    },
    {
        'akinsho/git-conflict.nvim',
        event = { 'BufReadPost', 'BufNewFile' },
        keys = {
            { '[x', function() require('git-conflict').find_prev('ours') end, desc = 'Previous git conflict' },
            { ']x', function() require('git-conflict').find_next('ours') end, desc = 'Previous git conflict' },
        },
        config = function()
            require('user.plugins.tools.git-conflict')
        end,
    },
    {
        'cshuaimin/ssr.nvim',
        keys = {
            { '<leader>ss', function() require('ssr').open() end, mode = { 'x', 'n' }, desc = 'Structural search/replace' },
        },
        opts = { border = vars.border },
    },
    {
        'nvim-pack/nvim-spectre',
        build = false,
        keys = {
            { '<leader>so', function() require('spectre').open() end, desc = 'Search/Replace (Spectre)' },
            { '<leader>so', function() require('spectre').open_visual() end, desc = 'Search/Replace selection (Spectre)', mode = 'x' },
            { '<leader>sp', function() require('spectre').open_file_search() end, desc = 'Search/Replace in current file' },
        },
        opts = { use_trouble_qf = true },
    },
    {
        'uga-rosa/ccc.nvim',
        cmd = { 'CccPick', 'CccConvert', 'CccHighlighterToggle' },
        keys = {
            { '<leader>ec', '<cmd>CccPick<CR>', desc = 'Pick color' },
            { '<leader>eC', '<cmd>CccPick<CR>', desc = 'Convert color under cursor' },
            { '<leader>uc', '<cmd>CccHighlighterToggle<CR>', desc = 'Highlight color text' },
        },
        config = function()
            require('user.plugins.tools.ccc')
        end,
    },
    {
        'folke/persistence.nvim',
        event = 'BufReadPre',
        keys = {
            { '<leader>ps', function() require('persistence').save() end, desc = 'Save session for current directory' },
            { '<leader>pd', function() require('persistence').stop() end, desc = 'Don\'t save current session' },
            { '<leader>pr', function() require('persistence').load() end, desc = 'Restore session' },
            { '<leader>pl', function() require('persistence').load { last = true } end, desc = 'Restore last session' },
        },
        opts = true,
    },
    {
        'michaelb/sniprun',
        build = 'cargo build --release',
        cmd = { 'SnipRun', 'SnipInfo' },
        keys = {
            { '<leader>ex', '<cmd>SnipRun<CR>', desc = 'Execute current line' },
            { '<leader>ex', '<cmd>SnipRun<CR>', desc = 'Execute selection', mode = 'x' },
        },
        opts = {
            display = { 'NvimNotify' },
            display_options = { notification_timeout = 2000 },
            show_no_output = {
                'Classic',
                'NvimNotify',
                'TempFloatingWindow',
            },
        },
    },
    {
        'mbbill/undotree',
        cmd = 'UndotreeToggle',
        keys = {
            { '<localleader>u', '<cmd>UndotreeToggle<CR>', desc = 'Undotree' },
        },
        init = function()
            vim.g.undotree_WindowLayout = 2
            vim.g.undotree_SplitWidth = 30
            vim.g.undotree_DiffpaneHeight = 10
            vim.g.undotree_SetFocusWhenToggle = 1
            vim.g.undotree_RelativeTimestamp = 1
        end,
    },
    {
        'potamides/pantran.nvim',
        cmd = 'Pantran',
        keys = {
            { '<leader>er', function() return require('pantran').motion_translate() end, expr = true, desc = 'Translate (motion)' },
            { '<leader>eR', function() return require('pantran').motion_translate() .. '_' end, expr = true, desc = 'Translate (cursor)' },
            { '<leader>er', function() return require('pantran').motion_translate() end, expr = true, mode = 'x', desc = 'Translate' },
        },
        opts = function()
            return require('user.plugins.tools.pantran')
        end,
    },
    {
        'iamcco/markdown-preview.nvim',
        build = 'cd app && pnpm install --prod',
        cmd = { 'MarkdownPreviewToggle', 'MarkdownPreview' },
        keys = {
            {
                '<localleader>p',
                ft = { 'markdown', 'rmd' },
                '<cmd>MarkdownPreviewToggle<CR>',
                desc = 'Preview Markdown file',
            },
        },
        opts = {
            mkdp_refresh_slow = 1,
            mkdp_filetypes = { 'markdown', 'rmd' },
            mkdp_echo_preview_url = 0,
            mkdp_preview_options = { disable_filename = 1 },
        },
        config = function(_, opts)
            for key, val in pairs(opts) do
                vim.g[key] = val
            end

            vim.cmd('do FileType')
        end,
    },
    {
        'mistricky/codesnap.nvim',
        build = 'make build_generator',
        cmd = { 'CodeSnap', 'CodeSnapSave' },
        opts = {
            mac_window_bar = false,
            watermark = '',
            bg_theme = 'summer',
            save_path = '/tmp/',
        },
    },
    {
        'jellydn/hurl.nvim',
        ft = 'hurl',
        keys = {
            { 'gu', '<cmd>HurlRunnerToEntry<CR>', desc = 'Run API request' },
            { 'gu', '<cmd>HurlRunner<CR>', desc = 'Run selected API request', mode = 'x' },
        },
        config = true,
    },
    {
        'vuki656/package-info.nvim',
        event = 'BufEnter package.json',
        config = function()
            require('user.plugins.tools.package-info')
        end,
    },
    {
        'dstein64/vim-startuptime',
        cmd = 'StartupTime',
        init = function()
            vim.g.startuptime_tries = 10
        end,
    },
}
