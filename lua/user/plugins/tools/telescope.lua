local telescope = require('telescope')
local border_style = require('user.config.vars').border
local layout = require('user.config.vars').telescope_layout

local borderchars = {
    horizontal = {
        single = { '─', '│', '─', '│', '┌', '┐', '┘', '└' },
        rounded = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
        double = { '═', '║', '═', '║', '╔', '╗', '╝', '╚' },
    },
    bottom_pane = {
        single = {
            prompt = { '─', '│', ' ', '│', '┌', '┐', ' ', ' ' },
            results = { '─', ' ', '─', '│', '├', '┤', '─', '└' },
            preview = { '─', '│', '─', '│', '┌', '┤', '┘', '┴' },
        },
        rounded = {
            prompt = { '─', '│', ' ', '│', '╭', '╮', ' ', ' ' },
            results = { '─', ' ', '─', '│', '├', '┤', '─', '╰' },
            preview = { '─', '│', '─', '│', '╭', '┤', '╯', '┴' },
        },
        double = {
            prompt = { '═', '║', ' ', '║', '╔', '╗', ' ', ' ' },
            results = { '═', ' ', '═', '║', '╠', '╣', '═', '╚' },
            preview = { '═', '║', '═', '║', '╔', '╣', '╝', '╩' },
        },
    },
    cursor = {
        single = {
            prompt = { '─', '│', ' ', '│', '┌', '┐', ' ', ' ' },
            results = { '─', '│', '─', '│', '├', '┤', '╯', '╰' },
            preview = { '─', '│', '─', '│', '┌', '┐', '┘', '└' },
        },
        rounded = {
            prompt = { '─', '│', ' ', '│', '╭', '╮', '│', '│' },
            results = { '─', '│', '─', '│', '├', '┤', '╯', '╰' },
            preview = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
        },
        double = {
            prompt = { '═', '║', ' ', '║', '╔', '╗', '║', '║' },
            results = { '═', '║', '═', '║', '╠', '╣', '╝', '╚' },
            preview = { '═', '║', '═', '║', '╔', '╗', '╝', '╚' },
        },
    },
}

local ignore_patterns = {
    '^%.bzr/',
    '^%.git/',
    '^%.hg/',
    '^%.pijul/',
    '^%.svn/',
    '^_darcs/',
    '^node_modules/',
    '^bazel%-out/',
    '^bazel%-bin/',
    '^bazel%-testlogs/',
    '^plz%-out/',
    '^%.plz%-cache/',
    '^%.plz%-http%-cache/',
}

-- Always search hidden files
local vimgrep_arguments = { unpack(require('telescope.config').values.vimgrep_arguments) }
table.insert(vimgrep_arguments, '--hidden')

telescope.setup {
    defaults = {
        prompt_prefix = '   ',
        selection_caret = ' ',
        sorting_strategy = 'ascending',
        mappings = {
            i = {
                ['<C-t>'] = require('trouble.sources.telescope').open,
                ['<A-j>'] = 'cycle_history_prev',
                ['<A-k>'] = 'cycle_history_next',
            },
            n = {
                ['q'] = 'close',
            },
        },
        dynamic_preview_title = true,
        layout_strategy = layout,
        layout_config = {
            bottom_pane = {
                height = 0.4,
                preview_width = 0.6,
            },
            horizontal = {
                prompt_position = 'top',
                preview_width = 0.6,
                height = 0.9,
                width = 0.9,
            },
            cursor = {
                width = 0.6,
                height = function(self, _, max_lines)
                    local results, PADDING = #self.finder.results, 4 -- this represents the size of the telescope window
                    local LIMIT = math.floor(max_lines / 2)
                    return (results <= (LIMIT - PADDING) and results + PADDING or LIMIT)
                end,
            },
        },
        borderchars = borderchars[layout][border_style],
        file_ignore_patterns = ignore_patterns,
        vimgrep_arguments = vimgrep_arguments,
    },
    pickers = {
        find_files = {
            hidden = true,
            follow = true,
        },
        live_grep = {
            -- ignore dependencies versions in lock files
            file_ignore_patterns = vim.list_extend(vim.list_slice(ignore_patterns), {
                '^go%.sum',
                '^Cargo%.lock',
                '^poetry%.lock',
                '^package%-lock%.json',
                '^yarn%.lock',
                '^pnpm%-lock%.yaml',
                '^flake%.lock',
            }),
        },
        registers = {
            layout_strategy = 'cursor',
            borderchars = borderchars.cursor[border_style],
            layout_config = { width = 0.4 },
        },
    },
    extensions = {
        file_browser = {
            hidden = {
                file_browser = true,
                folder_browser = true,
            },
            dir_icon = '󰉋',
        },
        fzf = { override_generic_sorter = false },
    },
}

-- NOTE: explicitly set after the setup so the options are applied correctly
telescope.load_extension('file_browser')
telescope.load_extension('fzf')
