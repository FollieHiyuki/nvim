local vars = require('user.config.vars')
local win_opts = {
    border = vars.border,
    win_options = { winblend = 0 },
}

return {
    columns = { 'icon', 'permissions' },
    cleanup_delay_ms = vim.opt.timeoutlen:get(),
    view_options = { show_hidden = true },
    float = win_opts,
    preview = win_opts,
    progress = win_opts,
}
