local ccc = require('ccc')

ccc.setup {
    default_color = '#000000',
    bar_char = '■',
    point_char = '◇',
    bar_len = 30,
    win_opts = {
        relative = 'cursor',
        row = 1,
        col = 1,
        style = 'minimal',
        border = require('user.config.vars').border,
    },
    inputs = {
        ccc.input.rgb,
        ccc.input.hsl,
        ccc.input.cmyk,
    },
    outputs = {
        ccc.output.hex,
        ccc.output.hex_short,
        ccc.output.css_rgb,
        ccc.output.css_hsl,
    },
    pickers = {
        ccc.picker.hex,
        ccc.picker.css_rgb,
        ccc.picker.css_hsl,
    },
    convert = {
        { ccc.picker.hex, ccc.output.css_rgb },
        { ccc.picker.css_rgb, ccc.output.css_hsl },
        { ccc.picker.css_hsl, ccc.output.hex },
    },
    save_on_quit = false,
    highlight_mode = 'bg',
}
