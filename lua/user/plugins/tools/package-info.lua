local isPackageJson = function()
    return vim.fn.fnamemodify(vim.api.nvim_buf_get_name(0), ':t') == 'package.json'
end

require('package-info').setup {
    autostart = false,
    hide_up_to_date = true,
    package_manager = 'pnpm',
    icons = {
        style = {
            up_to_date = ' ',
            outdated = '─ ',
            invalid = 'X ',
        },
    },
}

-- Register which-key menu entry and buffer mappings
require('which-key').add {
    { '<localleader>p', group = 'NPM package', cond = isPackageJson },
    {
        '<localleader>pt',
        function()
            require('package-info').toggle()
        end,
        desc = 'Toggle package information',
        cond = isPackageJson,
        buffer = true,
    },
    {
        '<localleader>pu',
        function()
            require('package-info').update()
        end,
        desc = 'Update package',
        cond = isPackageJson,
        buffer = true,
    },
    {
        '<localleader>pd',
        function()
            require('package-info').delete()
        end,
        desc = 'Delete package',
        cond = isPackageJson,
        buffer = true,
    },
    {
        '<localleader>pi',
        function()
            require('package-info').install()
        end,
        desc = 'Install new dependency',
        cond = isPackageJson,
        buffer = true,
    },
    {
        '<localleader>pc',
        function()
            require('package-info').change_version()
        end,
        desc = 'Change package version',
        cond = isPackageJson,
        buffer = true,
    },
}
