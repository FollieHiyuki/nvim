local filetypes = {
    'aerial',
    'NvimTree',
    'neo-tree',
    'neo-tree-popup',
    'neotest-summary',
    'startuptime',
    'toggleterm',
    'notify',
    'OverseerList',
    'gitcommit',
    'noice',
    'undotree',
}
local buftypes = { 'terminal', 'help', 'quickfix' }

-- Essentially a modified version of require('stickybuf').should_auto_pin()
return function(bufnr)
    local buftype = vim.bo[bufnr].buftype
    local filetype = vim.bo[bufnr].filetype
    local bufname = vim.api.nvim_buf_get_name(bufnr)

    if vim.tbl_contains(filetypes, filetype) then
        return 'filetype'
    elseif vim.tbl_contains(buftypes, buftype) then
        return 'buftype'
    elseif buftype == 'prompt' or vim.startswith(bufname, 'DAP ') then
        return 'bufnr'
    elseif bufname:match('Neogit.*Popup') then
        return 'bufnr'
    elseif filetype == 'NeogitStatus' or filetype == 'NeogitLog' or filetype == 'NeogitGitCommandHistory' then
        if vim.fn.winnr('$') > 1 then
            return 'filetype'
        end
    end
end
