require('project_nvim').setup {
    patterns = {
        '.bzr',
        '.git',
        '.hg',
        '.pijul',
        '.svn',
        '.fslckout',
        '_darcs',
        '>Code',
        'Makefile',
        'package.json',
        'go.mod',
    },
    show_hidden = true,
}
