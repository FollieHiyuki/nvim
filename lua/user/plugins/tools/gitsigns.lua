return {
    signs = {
        add          = { text = '+' },
        change       = { text = '~' },
        delete       = { text = '_' },
        topdelete    = { text = '‾' },
        changedelete = { text = '-' },
        untracked    = { text = '+' },
    },
    signs_staged_enable = false,
    attach_to_untracked = true,
    current_line_blame_opts = { virt_text_pos = 'right_align' },
    preview_config = { border = require('user.config.vars').border },
    on_attach = function(bufnr)
        local gs = require('gitsigns')
        local function map(mode, l, r, opts)
            opts = vim.tbl_extend('force', { noremap = true, silent = true }, opts)
            opts.buffer = bufnr
            vim.keymap.set(mode, l, r, opts)
        end

        -- stylua: ignore start
        map('n', ']g', function()
            if vim.wo.diff then
                return ']g'
            end
            vim.schedule(function()
                gs.next_hunk()
            end)
            return '<Ignore>'
        end, { expr = true, desc = 'Next hunk' })
        map('n', '[g', function()
            if vim.wo.diff then
                return '[g'
            end
            vim.schedule(function()
                gs.prev_hunk()
            end)
            return '<Ignore>'
        end, { expr = true, desc = 'Previous hunk' })
        map({ 'n', 'x' }, '<leader>gs', gs.stage_hunk, { desc = 'Stage hunk' })
        map({ 'n', 'x' }, '<leader>gr', gs.reset_hunk, { desc = 'Reset hunk' })
        map('n', '<leader>gp', gs.preview_hunk, { desc = 'Preview hunk' })
        map('n', '<leader>gu', gs.undo_stage_hunk, { desc = 'Undo staged hunk' })
        map('n', '<leader>gU', gs.reset_buffer_index, { desc = 'Reset buffer index' })
        map('n', '<leader>gS', gs.stage_buffer, { desc = 'Stage all hunks in buffer' })
        map('n', '<leader>gR', gs.reset_buffer, { desc = 'Reset all hunks in buffer' })
        map('n', '<leader>gb', function() gs.blame_line { full = true } end, { desc = 'Blame current line' })
        map('n', '<leader>gd', gs.diffthis, { desc = 'Show diff against current index' })
        map('n', '<leader>gD', function() gs.diffthis('~') end, { desc = 'Show diff against last commit' })
        map('n', '<leader>gz', gs.toggle_deleted, { desc = 'Toggle showing deleted hunk' })
        map({ 'o', 'x' }, 'ih', ':<C-u>Gitsigns select_hunk<CR>', { desc = 'Select git hunk' })
        map('n', 'gh', function() gs.setqflist('attached') end, { desc = 'Show git hunks in attached buffers' })
    end,
}
