local vars = require('user.config.vars')
local win_opts = {
    border = vars.border,
    win_opts = { winblend = 0 },
}

return {
    strategy = require('user.util.misc').has('toggleterm.nvim') and {
        'toggleterm',
        hidden = false,
        open_on_start = false,
        use_shell = false,
    } or 'terminal',
    auto_detect_success_color = false,
    default_template_prompt = 'missing',
    task_list = {
        default_detail = 2,
        max_width = { 120, 0.3 },
        min_width = { 40, 0.2 },
        direction = 'right',
    },
    form = win_opts,
    confirm = win_opts,
    task_win = win_opts,
    help_win = win_opts,
    dap = false, -- enable the integration manually
    log = {
        {
            type = 'echo',
            level = vim.log.levels.WARN,
        },
        {
            type = 'file',
            filename = 'overseer.log',
            level = vars.loglevel,
        },
    },
}
