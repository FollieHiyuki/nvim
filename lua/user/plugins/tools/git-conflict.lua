local gc = require('git-conflict')

gc.setup {
    disable_diagnostics = true,
    default_mappings = false,
    default_commands = false,
}

vim.api.nvim_create_autocmd('User', {
    pattern = 'GitConflictDetected',
    once = true,
    callback = function(event)
        local actions = {
            ['Base change'] = 'base',
            ['Both changes'] = 'both',
            ['Current change'] = 'ours',
            ['Incoming change'] = 'theirs',
            ['None of the changes'] = 'none',
        }

        vim.api.nvim_buf_create_user_command(event.buf, 'GitConflict', function()
            vim.ui.select(vim.tbl_keys(actions), { prompt = 'Select a conflict action' }, function(choice)
                if choice then gc.choose(actions[choice]) end
            end)
        end, { nargs = 0 })

        vim.keymap.set('n', '<localleader>c', '<cmd>GitConflict<CR>', {
            buffer = event.buf,
            noremap = true,
            silent = true,
            desc = 'Resolve git conflict',
        })
    end,
    desc = 'Custom user command for git-conflict',
    group = vim.api.nvim_create_augroup('UserGitConflict', {}),
})
