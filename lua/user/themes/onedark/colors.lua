local onedark = {
    black = '#282C34',
    grey1 = '#3E4452',
    grey2 = '#4B5263',
    grey3 = '#5C6470',
    grey_bright = '#73797E',
    fg = '#ABB2BF',
    white1 = '#BBC2CF',
    white2 = '#DFDFDF',
    teal = '#5699AF',
    cyan = '#56B6C2',
    blue = '#61AFEF',
    dark_blue = '#2257A0',
    red = '#E06C75',
    dark_red = '#BE5046',
    orange = '#D19A66',
    yellow = '#E5C07B',
    green = '#98C379',
    purple = '#C678DD',
    highlight = '#9CA0A4',
}

return onedark
