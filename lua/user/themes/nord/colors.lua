local nord = {
    black = '#2E3440',
    grey1 = '#3B4252',
    grey2 = '#434C5E',
    grey3 = '#4C566A',
    grey_bright = '#616E88',
    fg = '#D8DEE9',
    white1 = '#E5E9F0',
    white2 = '#ECEFF4',
    teal = '#8FBCBB',
    cyan = '#88C0D0',
    blue = '#81A1C1',
    dark_blue = '#5E81AC',
    red = '#BF616A',
    orange = '#D08770',
    yellow = '#EBCB8B',
    green = '#A3BE8C',
    purple = '#B48EAD',
    highlight = '#7B88A1',
}

return nord
