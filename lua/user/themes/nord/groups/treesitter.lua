local c = require('user.themes.nord.colors')

-- https://github.com/nvim-treesitter/nvim-treesitter/blob/master/CONTRIBUTING.md#highlights
-- :help treesitter-highlight-groups
return {
    -- Identifiers
    ['@variable'] = { link = 'Variable' },
    ['@variable.builtin'] = { fg = c.blue },
    ['@variable.parameter'] = { fg = c.purple, italic = true },
    ['@variable.member'] = { fg = c.teal },

    ['@constant'] = { link = 'Constant' },
    ['@constant.builtin'] = { fg = c.teal },
    ['@constant.macro'] = { fg = c.teal },

    ['@module'] = { fg = c.fg },
    ['@module.builtin'] = { fg = c.fg, bold = true },
    ['@label'] = { link = 'Label' },

    -- Literals
    ['@string'] = { link = 'String' },
    ['@string.documentation'] = { fg = c.yellow },
    ['@string.regexp'] = { fg = c.teal },
    ['@string.escape'] = { fg = c.purple },
    ['@string.special'] = { link = 'Special' },
    ['@string.special.symbol'] = { fg = c.purple },
    ['@string.special.path'] = { fg = c.green, underline = true },
    ['@string.special.url'] = { link = '@markup.link.url' },

    ['@character'] = { link = 'Character' },
    ['@character.special'] = { link = 'SpecialChar' },

    ['@boolean'] = { link = 'Boolean' },
    ['@number'] = { link = 'Number' },
    ['@number.float'] = { link = 'Float' },

    -- Types
    ['@type'] = { link = 'Type' },
    ['@type.builtin'] = { link = 'Type' },
    ['@type.definition'] = { link = 'Typedef' },
    ['@type.qualifier'] = { fg = c.blue, bold = true, italic = true },

    ['@attribute'] = { fg = c.purple },
    ['@attribute.builtin'] = { fg = c.purple, bold = true },
    ['@property'] = { fg = c.teal },

    -- Functions
    ['@function'] = { link = 'Function' },
    ['@function.builtin'] = { link = 'Function' },
    ['@function.call'] = { link = 'Function' },
    ['@function.macro'] = { fg = c.teal },

    ['@function.method'] = { fg = c.teal },
    ['@function.method.call'] = { fg = c.teal, bold = true },

    ['@constructor'] = { fg = c.blue },
    ['@operator'] = { link = 'Operator' },

    -- Keywords
    ['@keyword'] = { link = 'Keyword' },
    ['@keyword.coroutine'] = { link = 'Keyword' },
    ['@keyword.function'] = { link = 'Keyword' },
    ['@keyword.operator'] = { link = 'Operator' },
    ['@keyword.import'] = { link = 'Include' },
    ['@keyword.type'] = { link = 'Type' },
    ['@keyword.modifier'] = { link = 'Typedef' },
    ['@keyword.storage'] = { link = 'StorageClass' },
    ['@keyword.repeat'] = { link = 'Repeat' },
    ['@keyword.return'] = { link = 'Keyword' },
    ['@keyword.debug'] = { link = 'Debug' },
    ['@keyword.exception'] = { link = 'Exception' },

    ['@keyword.conditional'] = { link = 'Conditional' },
    ['@keyword.conditional.ternary'] = { link = 'Conditional' },

    ['@keyword.directive'] = { link = 'PreProc' },
    ['@keyword.directive.define'] = { link = 'Define' },

    -- Punctuation
    ['@punctuation.delimiter'] = { link = 'Delimiter' },
    ['@punctuation.bracket'] = { link = 'Delimiter' },
    ['@punctuation.special'] = { link = 'Delimiter' },

    -- Comments
    ['@comment'] = { link = 'Comment' },
    ['@comment.documentation'] = { fg = c.yellow, italic = true },

    ['@comment.error'] = { fg = c.red, bold = true },
    ['@comment.warning'] = { fg = c.yellow, bold = true },
    ['@comment.todo'] = { link = 'Todo' },
    ['@comment.note'] = { fg = c.cyan, bold = true },

    -- Markup
    ['@markup.strong'] = { bold = true },
    ['@markup.italic'] = { italic = true },
    ['@markup.strikethrough'] = { strikethrough = true },
    ['@markup.underline'] = { underline = true },

    ['@markup.heading'] = { fg = c.cyan, bold = true },
    ['@markup.heading.1'] = { fg = c.purple, bold = true },
    ['@markup.heading.2'] = { fg = c.blue, bold = true },
    ['@markup.heading.3'] = { fg = c.green, bold = true },
    ['@markup.heading.4'] = { fg = c.yellow, bold = true },
    ['@markup.heading.5'] = { fg = c.orange, bold = true },
    ['@markup.heading.6'] = { fg = c.fg, bold = true },

    ['@markup.quote'] = { fg = c.highlight, bold = true },
    ['@markup.math'] = { fg = c.yellow },
    ['@markup.environment'] = { fg = c.cyan },

    ['@markup.link'] = { fg = c.green },
    ['@markup.link.label'] = { link = 'Label' },
    ['@markup.link.url'] = { fg = c.green, underline = true },

    ['@markup.raw'] = { fg = c.green, italic = true },
    ['@markup.raw.block'] = { fg = c.green, italic = true },
    ['@markup.raw.delimiter'] = { link = 'Delimiter' },

    ['@markup.list'] = { link = 'Todo' },
    ['@markup.list.checked'] = { fg = c.green, bold = true },
    ['@markup.list.unchecked'] = { fg = c.blue, bold = true },

    ['@diff.plus'] = { link = 'DiffAdd' },
    ['@diff.minus'] = { link = 'DiffDelete' },
    ['@diff.delta'] = { link = 'DiffChange' },

    ['@tag'] = { link = 'Tag' },
    ['@tag.attribute'] = { fg = c.orange },
    ['@tag.delimiter'] = { fg = c.purple },

    -- Non-highlighting captures
    ['@conceal'] = { link = 'Conceal' },
}
