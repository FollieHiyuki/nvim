local c = require('user.themes.nord.colors')

-- :help group-name
return {
    Normal = { fg = c.fg },
    NormalFloat = { fg = c.fg },
    FloatBorder = { fg = c.white2 },
    ColorColumn = { bg = c.grey1 },
    Cursor = { fg = c.black, bg = c.fg },
    CursorIM = { fg = c.black, bg = c.white1 },
    CursorLine = { bg = c.grey1 },
    TermCursor = { link = 'Cursor' },
    TermCursorNC = { bg = c.grey1 },
    Underlined = { fg = c.green, underline = true },
    Ignore = { fg = c.grey1 },
    Error = { fg = c.fg, bg = c.red },
    LineNr = { fg = c.grey3 },
    MatchParen = { fg = c.cyan, bg = c.grey3 },
    NonText = { fg = c.highlight },
    Whitespace = { fg = c.highlight },
    EndOfBuffer = { fg = c.black }, -- hide filler lines with '~' completely
    Pmenu = { fg = c.fg, bg = c.grey1 },
    PmenuSbar = { fg = c.fg, bg = c.grey1 },
    PmenuSel = { fg = c.cyan, bg = c.grey3, bold = true },
    PmenuThumb = { fg = c.cyan, bg = c.grey3, bold = true },
    SpecialKey = { fg = c.grey3 },
    SperllBad = { fg = c.red, sp = c.red, undercurl = true },
    SpellCap = { fg = c.yellow, sp = c.yellow, undercurl = true },
    SpellLocal = { fg = c.white1, sp = c.white1, undercurl = true },
    SpellRare = { fg = c.white2, sp = c.white2, undercurl = true },
    Visual = { bg = c.grey2 },
    VisualNOS = { bg = c.grey2 },

    -- quickfix
    QuickFixLine = { bg = c.grey2 },
    qfLineNr = { fg = c.yellow },

    -- :checkhealth
    healthError = { fg = c.red, bg = c.grey1 },
    healthSuccess = { fg = c.green, bg = c.grey1 },
    healthWarning = { fg = c.yellow, bg = c.grey1 },

    -- gutter
    CursorColumn = { bg = c.grey1 },
    CursorLineNr = { fg = c.fg },
    Folded = { fg = c.highlight, bg = c.grey1 },
    FoldColumn = { fg = c.grey3 },
    SignColumn = { fg = c.grey1 },

    -- navigation
    Directory = { fg = c.cyan },

    -- prompt
    MsgArea = { link = 'Normal' },
    ErrorMsg = { fg = c.fg, bg = c.red },
    ModeMsg = { fg = c.fg },
    MoreMsg = { fg = c.cyan },
    Question = { fg = c.fg },
    WarningMsg = { fg = c.black, bg = c.yellow },
    WildMenu = { fg = c.cyan, bg = c.grey1 },

    -- statusline
    Statusline = { fg = c.cyan, bg = c.grey1 },
    StatusLineNC = { fg = c.fg, bg = c.grey1 },

    -- search
    IncSearch = { fg = c.white2, bg = c.dark_blue, underline = true },
    Search = { fg = c.grey1, bg = c.cyan },
    CurSearch = { link = 'IncSearch' },
    -- :s/../../
    Substitute = { link = 'IncSearch' },

    -- tabline
    TabLine = { fg = c.fg, bg = c.grey3 },
    TabLineFill = { fg = c.fg, bg = c.grey1 },
    TabLineSel = { fg = c.black, bg = c.cyan },

    -- winbar
    WinBar = { fg = c.fg, bg = 'NONE' },
    WinBarNC = { link = 'WinBar' },

    -- window
    Title = { fg = c.fg },
    WinSeparator = { fg = c.grey_bright },

    -- base syntax
    Boolean = { fg = c.blue },
    Character = { fg = c.fg },
    Comment = { fg = c.grey_bright, italic = true },
    Conceal = { fg = 'NONE', bg = 'NONE' },
    Conditional = { fg = c.blue, bold = true },
    Constant = { fg = c.yellow },
    Debug = { fg = c.highlight },
    Define = { fg = c.blue },
    Delimiter = { fg = c.white2 },
    Exception = { fg = c.blue, bold = true },
    Float = { fg = c.purple },
    Function = { fg = c.cyan },
    Identifier = { fg = c.fg },
    Include = { fg = c.blue },
    Keyword = { fg = c.blue, bold = true },
    Label = { fg = c.purple },
    Number = { fg = c.purple },
    Operator = { fg = c.blue },
    PreProc = { fg = c.blue },
    Repeat = { fg = c.blue, bold = true },
    Special = { fg = c.fg },
    SpecialChar = { fg = c.yellow },
    SpecialComment = { fg = c.cyan, italic = true },
    Statement = { fg = c.blue },
    StorageClass = { fg = c.blue },
    String = { fg = c.green },
    Structure = { fg = c.blue },
    Tag = { fg = c.fg },
    Todo = { fg = c.blue, bold = true },
    Type = { fg = c.blue },
    Typedef = { fg = c.blue },
    Macro = { link = 'Define' },
    PreCondit = { link = 'PreProc' },
    Variable = { link = 'Identifier' },

    -- diff
    DiffAdd = { fg = c.green, bg = c.grey1 },
    DiffChange = { fg = c.yellow, bg = c.grey1 },
    DiffDelete = { fg = c.red, bg = c.grey1 },
    DiffText = { fg = c.blue, bg = c.grey1 },

    -- git.vim / diff.vim builtin syntaxes
    diffAdded = { link = 'DiffAdd' },
    diffRemoved = { link = 'DiffDelete' },
    diffChanged = { link = 'DiffChange' },
    gitDate = { link = 'Constant' },

    -- NOTE: no TreeSitter grammars yet
    -- asciidoc
    asciidocAttributeEntry = { fg = c.purple, bold = true },
    asciidocAttributeList = { fg = c.purple, bold = true },
    asciidocAttributeRef = { fg = c.green, bold = true },
    asciidocHLabel = { fg = c.blue },
    asciidocListingBlock = { fg = c.teal },
    asciidocMacro = { fg = c.green },
    asciidocMacroAttributes = { fg = c.purple },
    asciidocOneLineTitle = { link = '@markup.heading' },
    asciidocPassthroughBlock = { fg = c.blue },
    asciidocTriplePlusPassthrough = { fg = c.teal },
    asciidocAdmonition = { link = 'Keyword' },
    asciidocBackslash = { link = 'Keyword' },
    asciidocQuotedBold = { bold = true },
    asciidocQuotedEmphasized = { italic = true },
    asciidocQuotedMonospaced = { fg = c.teal },
    asciidocQuotedMonospaced2 = { link = 'asciidocQuotedMonospaced' },
    asciidocQuotedUnconstrainedBold = { link = 'asciidocQuotedBold' },
    asciidocQuotedUnconstrainedEmphasized = { link = 'asciidocQuotedEmphasized' },
    asciidocURL = { link = '@markup.link.url' },
    asciidocEmail = { link = '@markup.link.url' },
}
