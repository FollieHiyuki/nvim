local c = require('user.themes.nord.colors')
local util = require('user.util.color')

return {
    -- Semantic tokens
    -- :help lsp-semantic-highlight
    ['@lsp.type.boolean'] = { link = '@boolean' },
    ['@lsp.type.builtinType'] = { link = '@type.builtin' },
    ['@lsp.type.class'] = { link = '@type' },
    ['@lsp.type.comment'] = { link = '@comment' },
    ['@lsp.type.enum'] = { link = '@type' },
    ['@lsp.type.enumMember'] = { link = '@constant' },
    ['@lsp.type.escapeSequence'] = { link = '@string.escape' },
    ['@lsp.type.formatSpecifier'] = { link = '@punctuation.special' },
    ['@lsp.type.field'] = { link = '@variable.member' },
    ['@lsp.type.function'] = { link = '@function' },
    ['@lsp.type.generic'] = { link = 'Normal' },
    ['@lsp.type.interface'] = { link = '@type' },
    ['@lsp.type.keyword'] = { link = '@keyword' },
    ['@lsp.type.macro'] = { link = '@function.macro' },
    ['@lsp.type.method'] = { link = '@function.method' },
    ['@lsp.type.namespace'] = { link = '@module' },
    ['@lsp.type.number'] = { link = '@number' },
    ['@lsp.type.operator'] = { link = '@operator' },
    ['@lsp.type.parameter'] = { link = '@variable.parameter' },
    ['@lsp.type.property'] = { link = '@property' },
    ['@lsp.type.selfKeyword'] = { link = '@variable.builtin' },
    ['@lsp.type.typeAlias'] = { link = '@type.definition' },
    ['@lsp.type.unresolvedReference'] = { fg = c.red, italic = true },
    ['@lsp.type.typeParameter'] = { link = '@variable.parameter' },
    ['@lsp.type.struct'] = { link = 'Structure' },
    ['@lsp.type.variable'] = { link = '@variable' },
    ['@lsp.typemod.enum.defaultLibrary'] = { link = '@type.builtin' },
    ['@lsp.typemod.enumMember.defaultLibrary'] = { link = '@constant.builtin' },
    ['@lsp.typemod.function.defaultLibrary'] = { link = '@function.builtin' },
    ['@lsp.typemod.macro.defaultLibrary'] = { link = '@function.builtin' },
    ['@lsp.typemod.method.defaultLibrary'] = { link = '@function.builtin' },
    ['@lsp.typemod.operator.injected'] = { link = '@operator' },
    ['@lsp.typemod.string.injected'] = { link = 'String' },
    ['@lsp.typemod.type.defaultLibrary'] = { link = '@type.builtin' },
    ['@lsp.typemod.variable.defaultLibrary'] = { link = '@variable.builtin' },
    ['@lsp.typemod.variable.injected'] = { link = '@variable' },
    ['@lsp.typemod.variable.static'] = { link = '@constant' },

    -- LSP
    -- :help diagnostic-highlights
    DiagnosticError = { fg = c.red },
    DiagnosticWarn = { fg = c.yellow },
    DiagnosticInfo = { fg = c.blue },
    DiagnosticHint = { fg = c.cyan },
    DiagnosticOk = { fg = c.green },
    DiagnosticUnderlineError = { sp = c.red, undercurl = true },
    DiagnosticUnderlineWarn = { sp = c.yellow, undercurl = true },
    DiagnosticUnderlineInfo = { sp = c.blue, undercurl = true },
    DiagnosticUnderlineHint = { sp = c.cyan, undercurl = true },
    DiagnosticVirtualTextError = { fg = c.red, bg = util.blend(c.red, c.black, 0.09), italic = true },
    DiagnosticVirtualTextWarn = { fg = c.yellow, bg = util.blend(c.yellow, c.black, 0.09), italic = true },
    DiagnosticVirtualTextInfo = { fg = c.blue, bg = util.blend(c.blue, c.black, 0.09), italic = true },
    DiagnosticVirtualTextHint = { fg = c.cyan, bg = util.blend(c.cyan, c.black, 0.09), italic = true },
    DiagnosticVirtualTextOk = { fg = c.green, bg = util.blend(c.green, c.black, 0.09), italic = true },
    -- :help lsp-highlight
    LspCodeLens = { link = 'Comment' },
    LspCodeLensSeparator = { fg = c.cyan, italic = true },
    LspInlayHint = { link = 'Comment' },
    LspSignatureActiveParameter = { bg = c.grey2 },
    LspInfoBorder = { link = 'FloatBorder' },
}
