local M = {}
local c = require('user.themes.nord.colors')

M.termcolors = {
    c.grey1,
    c.red,
    c.green,
    c.yellow,
    c.blue,
    c.purple,
    c.cyan,
    c.white1,
    c.grey_bright,
    c.red,
    c.green,
    c.yellow,
    c.blue,
    c.purple,
    c.teal,
    c.white2,
}

M.highlights = vim.tbl_extend(
    'force',
    require('user.themes.nord.groups.editor'),
    require('user.themes.nord.groups.treesitter'),
    require('user.themes.nord.groups.lsp'),
    require('user.themes.nord.groups.plugins')
)

return M
