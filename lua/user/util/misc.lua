local M = {}

function M.buffer_not_empty()
    if vim.fn.empty(vim.fn.expand('%:t')) ~= 1 then
        return true
    end
    return false
end

---@param plugin string
---@return boolean
function M.has(plugin)
    return require('lazy.core.config').plugins[plugin] ~= nil
end

---@param plugin string
---@return boolean
function M.loaded(plugin)
    local config = require('lazy.core.config')
    return config.plugins[plugin] and config.plugins[plugin]._.loaded
end

--- Switch between defined values of an option
---@param silent boolean?
---@param values? {[1]:any, [2]:any}
function M.toggle(option, silent, values)
    local util = require('lazy.core.util')
    if values then
        if vim.opt_local[option]:get() == values[1] then
            vim.opt_local[option] = values[2]
        else
            vim.opt_local[option] = values[1]
        end
        return util.info('Set ' .. option .. ' to ' .. vim.opt_local[option]:get(), { title = 'Vim option' })
    end

    vim.opt_local[option] = not vim.opt_local[option]:get()
    if not silent then
        if vim.opt_local[option]:get() then
            util.info('Enabled ' .. option, { title = 'Vim option' })
        else
            util.warn('Disabled ' .. option, { title = 'Vim option' })
        end
    end
end

--- Return the 1st found matched file/directory names
---@param files string|string[]
---@return fun(src: string): nil|string
function M.root_has_file(files)
    return function(src)
        local found = vim.fs.find(files, { upward = true, path = src })[1]
        if found then
            return vim.fs.dirname(found)
        end
    end
end

return M
