local M = {}

--- Convert Hexadecimal color string to list of decimal Red, Green, Blue values
---@param color string e.g. '#2e3440'
---@return table
local function hex2rgb(color)
    color = string.lower(color)
    return {
        tonumber(color:sub(2, 3), 16),
        tonumber(color:sub(4, 5), 16),
        tonumber(color:sub(6, 7), 16),
    }
end

--- Lighten/Darken the colors with the amount provided
---@param hex string e.g. '#2e3440'
---@param alpha number a number in the range [-1, 1]
---@return string
function M.shade(hex, alpha)
    vim.validate {
        percent = { alpha, function(arg) return -1 <= arg and arg <= 1 end, 'a number in the range [-1, 1]' }
    }

    local rgb = hex2rgb(hex)
    local function shadeChannel(i)
        return math.min(math.floor(math.max(0, rgb[i] * (1 + alpha))), 255)
    end

    return string.format('#%02x%02x%02x', shadeChannel(1), shadeChannel(2), shadeChannel(3))
end

---@param foreground string foreground color
---@param background string background color
---@param alpha number number within range [-1, 1]
function M.blend(foreground, background, alpha)
    vim.validate {
        percent = { alpha, function(arg) return -1 <= arg and arg <= 1 end, 'a number in the range [-1, 1]' }
    }

    local fg = hex2rgb(foreground)
    local bg = hex2rgb(background)

    local blendChannel = function(i)
        local ret = (alpha * fg[i] + (1 - alpha) * bg[i])
        return math.floor(math.min(math.max(0, ret), 255) + 0.5)
    end

    return string.format('#%02x%02x%02x', blendChannel(1), blendChannel(2), blendChannel(3))
end

function M.darken(hex, alpha, bg)
    return M.blend(hex, alpha, bg or '#000000')
end

function M.lighten(hex, alpha, fg)
    return M.blend(hex, alpha, fg or '#ffffff')
end

--- Load defined Neovim theme inside lua/user/themes/ and setup highlights
---@param theme string
function M.load_theme(theme)
    -- Reset
    if vim.g.colors_name then
        vim.cmd('highlight clear')
    end

    -- Get theme specs
    vim.g.colors_name = theme
    local t = require('user.themes.' .. theme)

    -- Set base16 terminal colors
    for index, val in ipairs(t.termcolors) do
        vim.g['terminal_color_' .. (index - 1)] = val
    end

    -- Load highlight groups
    for group, val in pairs(t.highlights) do
        vim.api.nvim_set_hl(0, group, val)
    end
end

return M
