local M = {}
local lazy_util = require('lazy.core.util')

--- Return the list of active LSP clients
---@return table
function M.active_lsp_clients(opts)
    local clients = vim.lsp.get_clients(opts)
    if opts and opts.method then
        clients = vim.tbl_filter(function(client)
            return client.supports_method(opts.method, { bufnr = opts.bufnr })
        end, clients)
    end

    return (opts and opts.filter) and vim.tbl_filter(opts.filter, clients) or clients
end

--- Return the list of active nvim-lint linters for the current buffer
---@return table
function M.active_linters(bufnr)
    local lint = require('lint')
    local ft = vim.api.nvim_get_option_value('filetype', { buf = bufnr })

    local linters = vim.list_slice(lint._resolve_linter_by_ft(ft))

    -- Add global and fallback linters
    if #linters == 0 then
        vim.list_extend(linters, lint.linters_by_ft['_'] or {})
    end
    vim.list_extend(linters, lint.linters_by_ft['*'] or {})

    -- Filtering based on provided condition field
    local filename = vim.api.nvim_buf_get_name(bufnr)
    linters = vim.tbl_filter(function(name)
        local conf = lint.linters[name]
        if not conf then
            lazy_util.warn('Linter not found: ' .. name, { title = 'nvim-lint' })
        end
        return conf and not (type(conf) == 'table' and conf.condition and not conf.condition(filename))
    end, linters)

    return linters
end

-- Re-implement nvim-lightbulb naively
-- Update status text indicating whether a CodeAction exists at cursor position
function M.codeAction_on_attach(bufnr)
    local params = vim.lsp.util.make_range_params()
    params.context = { diagnostics = vim.lsp.diagnostic.get_line_diagnostics(bufnr) }
    vim.lsp.buf_request_all(bufnr, 'textDocument/codeAction', params, function(responses)
        local has_actions = false
        for _, resp in pairs(responses) do
            if resp.result and not vim.tbl_isempty(resp.result) then
                has_actions = true
                break
            end
        end

        if has_actions then
            vim.b.has_code_action_text = '󰌵 Code action'
        else
            vim.b.has_code_action_text = nil
        end
    end)
end

return M
