local group = vim.api.nvim_create_augroup('UserAutoCmds', {})
local function close_with_q(buf)
    vim.bo[buf].buflisted = false
    vim.keymap.set('n', 'q', '<cmd>close<CR>', { buffer = buf, silent = true })
end

-- Close some filetypes with <q> and auto delete buffer for them
vim.api.nvim_create_autocmd('FileType', {
    pattern = {
        'qf',
        'help',
        'man',
        'query',
        'notify',
        'spectre_panel',
        'neotest-output',
        'neotest-output-panel',
        'neotest-summary',
        'startuptime',
        'PlenaryTestPopup',
    },
    callback = function(args)
        close_with_q(args.buf)
    end,
    group = group,
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = { 'checkhealth', 'log' },
    callback = function(args)
        -- Only do this to Neovim related log files
        if vim.api.nvim_get_option_value('filetype', { buf = args.buf }) == 'log'
            and vim.fn.expand('%:p:h') ~= vim.fn.stdpath('log')
        then
            return
        end
        vim.bo[args.buf].bufhidden = 'delete'
        close_with_q(args.buf)
    end,
    group = group,
})
vim.api.nvim_create_autocmd('FileType', {
    pattern = { 'gitcommit', 'gitrebase' },
    callback = function(args)
        vim.bo[args.buf].bufhidden = 'wipe'
        vim.opt_local.colorcolumn = { 80 }
    end,
    group = group,
})

-- Show cursor line only in active window
vim.api.nvim_create_autocmd('WinEnter', {
    callback = function()
        local ok, cl = pcall(vim.api.nvim_win_get_var, 0, 'cursorline')
        if ok and cl then
            vim.wo.cursorline = true
            vim.api.nvim_win_del_var(0, 'cursorline')
        end
    end,
    group = group,
})
vim.api.nvim_create_autocmd('WinLeave', {
    callback = function()
        local cl = vim.wo.cursorline
        if cl then
            vim.api.nvim_win_set_var(0, 'cursorline', cl)
            vim.wo.cursorline = false
        end
    end,
    group = group,
})

-- No undo history for temporary files
vim.api.nvim_create_autocmd('BufWritePre', {
    pattern = { '/tmp/*', 'COMMIT_EDITMSG', 'MERGE_MSG', '*.tmp', '*.bak' },
    callback = function()
        vim.opt_local.undofile = false
    end,
    group = group,
})

-- Equalize window sizes when resizing the terminal
vim.api.nvim_create_autocmd('VimResized', {
    pattern = '*',
    command = 'tabdo wincmd =',
    group = group,
})

-- Force writing shada file on leaving
vim.api.nvim_create_autocmd('VimLeave', {
    pattern = '*',
    command = 'if has("nvim") | wshada! | else | wviminfo! | endif',
    group = group,
})

-- Check if we need to reload the file when it changed
vim.api.nvim_create_autocmd({ 'FocusGained', 'TermClose', 'TermLeave' }, {
    pattern = '*',
    command = 'checktime',
    group = group,
})

-- Highlight region on yank
vim.api.nvim_create_autocmd('TextYankPost', {
    pattern = '*',
    callback = function()
        vim.highlight.on_yank { higroup = 'IncSearch', timeout = 300 }
    end,
    group = group,
})

-- Wrap in Telescope's preview buffer
vim.api.nvim_create_autocmd('User', {
    pattern = 'TelescopePreviewerLoaded',
    callback = function()
        vim.opt_local.wrap = true
    end,
    group = group,
})
