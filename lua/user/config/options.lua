local global_vars = {
    -- Leader keys
    mapleader = ' ',
    maplocalleader = '\\',

    -- TeX flavor
    tex_flavor = 'latex',
}

local vim_options = {
    -- General settings
    mouse = 'nv',
    confirm = true,
    fileformats = 'unix',
    virtualedit = 'block',
    fileencoding = 'utf-8',
    clipboard = { 'unnamed', 'unnamedplus' },
    wildignorecase = true,
    wildignore = {
        '.DS_Store',
        '.git',
        '.hg',
        '.svn',
        '*.pyc',
        '*.o',
        '*.obj',
        '*.out',
        '*.ico',
        '*.jar',
        '*.jpg',
        '*.jpeg',
        '*.png',
        '*.gif',
        '*.avi',
        '*.wav',
        '*.zip',
        '**/tmp/**',
        '**/node_modules/**',
        '**/bazel-out/**',
        '**/bazel-bin/**',
        '**/bazel-testlogs/**',
        '**/plz-out/**',
        '**/.plz-cache/**',
        '**/.plz-http-cache/**',
    },
    showmode = false,
    sessionoptions = { 'buffers', 'curdir', 'tabpages', 'winsize' },
    jumpoptions = 'stack',
    formatoptions = '1jcroqlnt',
    wrap = false,
    sidescrolloff = 4,
    scrolloff = 4,
    whichwrap = {
        ['<'] = true,
        ['>'] = true,
        ['['] = true,
        [']'] = true,
        ['~'] = true,
    },
    splitkeep = 'screen',
    -- colorcolumn = { 120 },
    -- cursorcolumn = true,
    cursorline = true,
    cursorlineopt = 'number',
    showcmd = false,
    cmdheight = 0,
    showtabline = 2,
    laststatus = 3,
    synmaxcol = 2500,
    grepformat = '%f:%l:%c:%m',
    grepprg = 'rg --hidden --color=never --vimgrep --smart-case --',
    diffopt = { 'filler', 'iwhite', 'internal', 'algorithm:patience' },

    -- Spell checking, dictionary
    -- spell = true,
    -- spellfile = vim.fn.stdpath('config') .. '/spell/en.utf-8.add',
    -- dictionary = { '/usr/share/dict/american-english' },

    -- Conceal
    conceallevel = 2,
    concealcursor = 'nc',
    foldenable = true,
    foldlevelstart = 99,
    foldmethod = 'expr',
    foldexpr = 'v:lua.vim.treesitter.foldexpr()',

    -- Case insensitive
    ignorecase = true,
    smartcase = true,
    infercase = true,

    -- Update time
    timeoutlen = 500,
    ttimeoutlen = 25,
    updatetime = 500,
    redrawtime = 1500,

    -- No swapfile
    writebackup = false,
    swapfile = false,

    -- Completion menu
    wildmode = 'longest:full,full',
    complete = { '.', 'w', 'b', 'k' },
    completeopt = { 'menu', 'menuone', 'noselect' },
    pumheight = 16,

    -- Window rules
    splitbelow = true,
    splitright = true,
    switchbuf = 'useopen',
    winwidth = 30,
    winminwidth = 10,
    equalalways = false,

    -- Left column
    signcolumn = 'auto:6',
    number = true,
    relativenumber = true,
    -- numberwidth = 6,

    -- 4 spaces = 1 tab
    tabstop = 4,
    -- softtabstop = -1, -- use the value of 'shiftwidth'
    shiftwidth = 0, -- use the value of 'tabstop'
    expandtab = true,
    smartindent = true,
    shiftround = true,

    -- Trailings, line break
    list = true,
    listchars = {
        tab = '»·',
        nbsp = '+',
        trail = '·',
        extends = '→',
        precedes = '←',
    },
    showbreak = '↳  ',
    -- linebreak = true,
    -- breakat = [[\ \	;:,!?]],
    breakindent = true,
    -- breakindentopt = { shift = 4, min = 20 },

    -- Undo file path
    undofile = true,
    undolevels = 10000,
}

-- Load global variables
for k, v in pairs(global_vars) do
    vim.g[k] = v
end

-- Load editor options
for k, v in pairs(vim_options) do
    vim.opt[k] = v
end

-- Special treats
vim.opt.shortmess:append { W = true, I = true, c = true, C = true }
