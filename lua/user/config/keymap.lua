local util = require('user.util.misc')
local map = vim.keymap.set

-- better up/down
map({ 'n', 'x' }, 'j', 'v:count == 0 ? "gj" : "j"', { expr = true, silent = true })
map({ 'n', 'x' }, 'k', 'v:count == 0 ? "gk" : "k"', { expr = true, silent = true })

-- better indenting
map('v', '<', '<gv')
map('v', '>', '>gv')

-- remove trailing whitespaces
map('n', '<localleader>w', '<cmd>%s/\\s\\+$//e<CR>', { desc = 'Remove whitespaces' })

-- Resize window using <ctrl> + arrow keys
map('n', '<C-Up>', '<cmd>resize +2<CR>', { desc = 'Increase window height' })
map('n', '<C-Down>', '<cmd>resize -2<CR>', { desc = 'Decrease window height' })
map('n', '<C-Left>', '<cmd>vertical resize -2<CR>', { desc = 'Decrease window width' })
map('n', '<C-Right>', '<cmd>vertical resize +2<CR>', { desc = 'Increase window width' })

-- H, L instead of 0, $
-- map({ 'n', 'x', 'o' }, 'H', '0')
-- map({ 'n', 'x', 'o' }, 'L', '$')

-- Clear search with <Esc>
map({ 'i', 'n' }, '<Esc>', '<cmd>noh<CR><Esc>', { desc = 'Escape and clear hlsearch' })

-- More intuitive n/N direction
-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
map('n', 'n', '"Nn"[v:searchforward]', { expr = true, desc = 'Next search result' })
map('x', 'n', '"Nn"[v:searchforward]', { expr = true, desc = 'Next search result' })
map('o', 'n', '"Nn"[v:searchforward]', { expr = true, desc = 'Next search result' })
map('n', 'N', '"nN"[v:searchforward]', { expr = true, desc = 'Prev search result' })
map('x', 'N', '"nN"[v:searchforward]', { expr = true, desc = 'Prev search result' })
map('o', 'N', '"nN"[v:searchforward]', { expr = true, desc = 'Prev search result' })

-- switching between buffers
if not util.has('nvim-cokeline') then
    map('n', ']b', '<cmd>bnext<CR>', { desc = 'Next buffer' })
    map('n', '[b', '<cmd>bprevious<CR>', { desc = 'Previous buffer' })
end

-- move between quickfix items
if not util.has('trouble.nvim') then
    map('n', '[q', vim.cmd.cprev, { desc = 'Previous quickfix' })
    map('n', ']q', vim.cmd.cnext, { desc = 'Next quickfix' })
end

-- LSP keymaps that can be used without an LSP server attached to the buffer (.e.g with nvim-lint)
local diagnostic_goto = function(next, severity)
    local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
    severity = severity and vim.diagnostic.severity[severity] or nil
    return function()
        go { severity = severity }
    end
end
map('n', '<leader>le', vim.diagnostic.open_float, { desc = 'Line diagnostic' })
map('n', '<leader>ld', '<cmd>Trouble diagnostics toggle filter.buf=0<CR>', { desc = 'Buffer diagnostics' })
map('n', '<leader>lw', '<cmd>Trouble diagnostics toggle<CR>', { desc = 'Workspace diagnostics' })
map('n', ']d', diagnostic_goto(true), { desc = 'Next diagnostic' })
map('n', '[d', diagnostic_goto(false), { desc = 'Previous diagnostic' })
map('n', ']e', diagnostic_goto(true, 'ERROR'), { desc = 'Next error' })
map('n', '[e', diagnostic_goto(false, 'ERROR'), { desc = 'Previous error' })
map('n', ']w', diagnostic_goto(true, 'WARN'), { desc = 'Next warning' })
map('n', '[w', diagnostic_goto(false, 'WARN'), { desc = 'Previous warning' })

-- toggle LSP settings
map('n', '<leader>ud', require('user.plugins.lsp.lspconfig.diagnostic').toggle, { desc = 'Toggle diagnostics' })
map('n', '<leader>uv', require('user.plugins.lsp.lspconfig.diagnostic').toggle_virtual_lines, { desc = 'Toggle virtual lines diagnostics' })

-- toggle editor settings
map('n', '<leader>ur', function() util.toggle('relativenumber', true) end, { desc = 'Toggle relativenumber' })
map('n', '<leader>ue', function() util.toggle('conceallevel', false, { 0, vim.go.conceallevel > 0 and vim.go.conceallevel or 3 }) end, { desc = 'Toggle conceallevel' })
map('n', '<leader>us', function() util.toggle('expandtab') end, { desc = 'Toggle tab/space indent' })

-- exit
-- map('n', '<leader>qw', '<C-w>c', { desc = 'Delete window' })
-- map('n', '<leader>qq', '<cmd>qa<CR>', { desc = 'Quit all' })

-- get highlight groups under cursor
map('n', '<leader>up', vim.show_pos, { desc = 'Inspect pos' })
map('n', '<leader>ut', vim.treesitter.inspect_tree, { desc = 'Inspect treesitter' })

-- escape in terminal mode
map('t', '<Esc><Esc>', '<C-\\><C-n>', { desc = 'Enter Normal mode' })

-- easier window split
map('n', '<leader>-', '<C-w>s', { desc = 'Split window below' })
map('n', '<leader>|', '<C-w>v', { desc = 'Split window right' })

-- tabs
map('n', '<leader><Tab>l', '<cmd>tablast<CR>', { desc = 'Last tab' })
map('n', '<leader><Tab>f', '<cmd>tabfirst<CR>', { desc = 'First tab' })
map('n', '<leader><Tab><Tab>', '<cmd>tabnew<CR>', { desc = 'New tab' })
map('n', '<leader><Tab>]', '<cmd>tabnext<CR>', { desc = 'Next tab' })
map('n', '<leader><Tab>d', '<cmd>tabclose<CR>', { desc = 'Close tab' })
map('n', '<leader><Tab>[', '<cmd>tabprevious<CR>', { desc = 'Previous tab' })
map('n', '<leader><Tab>n', '<cmd>+tabmove<CR>', { desc = 'Move tab next' })
map('n', '<leader><Tab>p', '<cmd>-tabmove<CR>', { desc = 'Move tab previous' })

-- open Lazy UI
map('n', '<leader><Space>', '<cmd>Lazy<CR>', { desc = 'Lazy' })
