local M = {}

M.border = 'rounded'

-- Log level used for nvim-notify and LSP log
M.loglevel = vim.log.levels.INFO

-- Some default LSP options (can be toggled)
M.lsp_format_on_save = false
M.lsp_virtual_lines = false

-- Only 'bottom_pane', 'horizontal' or 'cursor' right now
M.telescope_layout = 'horizontal'

-- 'tabby' or 'cokeline'
M.tabline_provider = 'cokeline'

-- 'nvim-tree' or 'neo-tree'
M.filetree_provider = 'neo-tree'

-- Where to install tree-sitter parsers (will be added to rtp)
M.ts_parsers_path = vim.fn.stdpath('data') .. '/parsers'

M.icons = {
    notify = { -- also used for diagnostic signs
        error = '',
        hint = '',
        info = '',
        warn = '',
        debug = '',
        trace = '',
    },
    git  = {
        added = ' ',
        modified = ' ',
        removed = ' ',
    },
    dap = {
        Stopped = { ' ', 'DiagnosticWarn', 'Visual' },
        Breakpoint = ' ',
        BreakpointCondition = { ' ', 'DiagnosticHint' },
        BreakpointRejected = { ' ', 'DiagnosticError' },
        LogPoint = ' ',
    },
    kind = {
        -- aerial.nvim, neotree
        File = { icon = '', hl = '@string.special.path' },
        Module = { icon = '', hl = '@module' },
        Namespace = { icon = '', hl = '@module' },
        Package = { icon = '', hl = '@module' },
        Class = { icon = '', hl = '@keyword.storage' },
        Method = { icon = '', hl = '@function.method' },
        Property = { icon = '', hl = '@property' },
        Field = { icon = '', hl = '@variable.member' },
        Constructor = { icon = '󰒓', hl = '@constructor' },
        Enum = { icon = '', hl = '@lsp.type.enum' },
        Interface = { icon = '', hl = '@lsp.type.interface' },
        Function = { icon = '󰡱', hl = '@function' },
        Variable = { icon = '', hl = '@variable' },
        Constant = { icon = '', hl = '@constant' },
        String = { icon = '', hl = '@string' },
        Number = { icon = '', hl = '@number' },
        Boolean = { icon = '', hl = '@boolean' },
        Array = { icon = '', hl = '@constant' },
        Object = { icon = '', hl = '@type' },
        Key = { icon = '󰌋', hl = '@type' },
        Null = { icon = '󰟢', hl = '@none' },
        EnumMember = { icon = '', hl = '@constant' },
        Struct = { icon = '', hl = '@lsp.type.struct' },
        Event = { icon = '', hl = '@type' },
        Operator = { icon = '', hl = '@operator' },
        TypeParameter = { icon = '', hl = '@variable.parameter' },

        -- Additional ones for nvim-cmp.
        -- No need to specify the highlights here.
        Text = { icon = '' },
        Unit = { icon = '' },
        Value = { icon = ' ' },
        Keyword = { icon = '' },
        Snippet = { icon = '' },
        Color = { icon = '' },
        Reference = { icon = '' },
        Folder = { icon = '' },
    },
}

return M
