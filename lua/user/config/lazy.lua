local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.uv.fs_stat(lazypath) then
    vim.fn.system {
        'git',
        'clone',
        '--filter=blob:none',
        'https://github.com/folke/lazy.nvim.git',
        lazypath,
    }

    -- Use specific commit of lazy.nvim if defined
    local f = io.open(vim.fn.stdpath('config') .. '/lazy-lock.json', 'r')
    if f then
        local lock = vim.json.decode(f:read('*a'))
        vim.fn.system { 'git', '-C', lazypath, 'checkout', lock['lazy.nvim'].commit }
    end
end
vim.opt.rtp:prepend(lazypath)

local opts = {
    install = { colorscheme = { 'nord', 'onedark', 'habamax' } },
    defaults = { lazy = true },
    ui = {
        wrap = false,
        size = { width = 0.9, height = 0.85 },
        border = require('user.config.vars').border,
    },
    checker = { enabled = false, notify = false },
    change_detection = { enabled = true, notify = false },
    rocks = { enabled = false },
    performance = {
        cache = { enabled = true },
        reset_packpath = true,
        rtp = {
            reset = true,
            disabled_plugins = {
                'gzip',
                'matchit',
                'matchparen',
                'netrwPlugin',
                'rplugin',
                'spellfile',
                'tarPlugin',
                'tohtml',
                'tutor',
                'zipPlugin',
            },
        },
    },
}

require('lazy').setup('user.plugins', opts)
