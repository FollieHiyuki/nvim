-- Configurations
require('user.config.options')

-- Load plugins with lazy.nvim
require('user.config.lazy')

-- Keymaps and Autocmds can wait
vim.api.nvim_create_autocmd('User', {
    pattern = 'VeryLazy',
    once = true,
    callback = function()
        require('user.config.autocmds')
        require('user.config.keymap')
    end,
    group = vim.api.nvim_create_augroup('UserLazyConfig', {}),
})
