vim.filetype.add {
    filename = {
        vifmrc = 'vifm',
        ['.ansible-lint'] = 'yaml',
        ['.yamllint'] = 'yaml',
        ['.gitlab-ci.yml'] = 'yaml.gitlab',
        ['ansible.cfg'] = 'ini',
        ['.npmrc'] = 'ini',
        ['BUCK'] = 'bzl',
        ['Tiltfile'] = 'tiltfile',
        ['flake.lock'] = 'json',
        ['deno.lock'] = 'json',
        ['MODULE.bazel.lock'] = 'json',
        ['terraform.tfstate'] = 'json',
    },
    extension = {
        dj = 'djot',
        gwl = 'wisp',
        j2 = 'jinja',
        jinja = 'jinja',
        jinja2 = 'jinja',
        k = 'kcl',
        kk = 'koka',
        mdx = 'markdown.mdx',
        ncl = 'nickel',
        purs = 'purescript',
        rasi = 'rasi',
        rasinc = 'rasi',
        river = 'river',
        tmpl = 'gotmpl',
        vifm = 'vifm',
        w = 'wisp',
        wisp = 'wisp',
    },
    pattern = {
        ['${HOME}/.kube/config'] = 'yaml',
        -- Ansible
        ['.*/roles/[^/]+/tasks/.+%.ya?ml'] = 'yaml.ansible',
        ['.*/roles/[^/]+/handlers/[^/]+%.ya?ml'] = 'yaml.ansible',
        ['.*/playbooks/[^/]+%.ya?ml'] = 'yaml.ansible',
        -- Helm chart templates
        ['.*/templates/.+%.ya?ml'] = 'helm',
        ['.*/templates/.+%.tpl'] = 'helm',
        -- please.build
        ['%.?plzconfig'] = 'gitconfig',
        ['%.plzconfig_[%w_]+'] = 'gitconfig',
        ['%.plzconfig.[%w_%-]+'] = 'gitconfig',
        -- Fallback logic
        ['.*'] = {
            priority = -math.huge,
            function(_, bufnr)
                local first_line = vim.api.nvim_buf_get_lines(bufnr, 0, 1, false)[1] or ''
                if first_line:find('^#!.*[%s/]nft %-f$') then
                    return 'nftables'
                end
            end,
        },
    },
}
