# folliehiyuki's Neovim configuration

This Neovim config used to live within [my dotfiles](/FollieHiyuki/dotfiles-ansible). Due to the complexity of refactoring both regular files/templates and this Lua codebase, I decided to move it into its own repository. As such, this project stays under the same license as my dotfiles, MIT.

## TODO

### Colorschemes

- OneDark
- Catpuccin

### Additional DAP/LSP servers

- elixirls/lexical, elp/erlangls (config for `nvim-lspconfig`)
- one-small-step-for-vimkind
- cmake-language-server / neocmakelsp
- dart/flutter (flutter-tools.nvim / configure dartls directly) (ref: <https://gist.github.com/christopherfujino/80be0f4cd88f75c4991b478e6b071153>)
- metals (consider `nvim-metals`)
- regal DAP server
- Replace `conform.nvim` / `nvim-lint` with [efm-langserver](https://github.com/mattn/efm-langserver)?
- harper_ls
- jqls
- rust_analyzer
- [kulala-ls](https://github.com/mistweaverco/kulala-ls)

### More plugins

- [clangd_extensions.nvim](https://github.com/p00f/clangd_extensions.nvim)
- [crates.nvim](https://github.com/Saecki/crates.nvim)
- [numb.nvim](https://github.com/nacro90/numb.nvim)
- [nvim-ufo](https://github.com/kevinhwang91/nvim-ufo)
- [refactoring.nvim](https://github.com/ThePrimeagen/refactoring.nvim)
- [rustaceanvim](https://github.com/mrcjkb/rustaceanvim)
- [telekasten.nvim](https://github.com/renerocksai/telekasten.nvim) / [zk-nvim](https://github.com/zk-org/zk-nvim)
- [tangerine.nvim](https://github.com/udayvir-singh/tangerine.nvim) / [aniseed](https://github.com/Olical/aniseed) + [nfnl](https://github.com/Olical/nfnl) / [hotpot.nvim](https://github.com/rktjmp/hotpot.nvim)
- [nabla.nvim](https://github.com/jbyuki/nabla.nvim)
- markdown-preview.nvim -> [peek.nvim](https://github.com/toppair/peek.nvim) / [smp.nvim](https://github.com/cnshsliu/smp.nvim) / [previm](https://github.com/previm/previm)
- [grpc-nvim](https://github.com/hudclark/grpc-nvim)
- [nvim-ide](https://github.com/ldelossa/nvim-ide)
- [haskell-tools.nvim](https://github.com/mrcjkb/haskell-tools.nvim)
- [nvim-asciidoc-preview](https://github.com/tigion/nvim-asciidoc-preview)
- [lsp-lens.nvim](https://github.com/VidocqH/lsp-lens.nvim)
- [venv-selector.nvim](https://github.com/linux-cultist/venv-selector.nvim)
- [virtual-types.nvim](https://github.com/jubnzv/virtual-types.nvim)
- [hover.nvim](https://github.com/lewis6991/hover.nvim)
- [otter.nvim](https://github.com/jmbuhr/otter.nvim)
- [nvim-rulebook](https://github.com/chrisgrieser/nvim-rulebook)
- [lazy-nix-helper.nvim](https://github.com/b-src/lazy-nix-helper.nvim) (to turn this repo into a proper Nix Flake)
- nvim-web-devicons -> mini.icons (when it's more stable and widely adopted)
- nvim-spectre -> grug-far.nvim
- decisive.nvim
- headlines.nvim -> render-markdown.nvim
- markdown-preview.nvim -> https://github.com/jannis-baum/vivify
- https://github.com/max397574/care.nvim
- trouble.nvim -> quicker.nvim???
- stickybuf.nvim -> [window local option `winfixbuf`](https://github.com/neovim/neovim/issues/12517)
- https://github.com/nvim-orgmode/orgmode#plugins
- nvim-cmp -> blink.cmp
- snacks.nvim (to replace a bunch of other plugins)
- nvim-origami
- remote-nvim.nvim

### Logic

- Copy sensible values from <https://github.com/echasnovski/mini.nvim/blob/main/lua/mini/basics.lua> and <https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-misc.md>
  - Use `setup_auto_root()` to replace project.nvim
  - Might need a ~better~ simpler project manager (.e.g cd-project.nvim, telescope-project.nvim)
- Remove `nvim-lspconfig` plugin after <https://github.com/neovim/neovim/issues/28479>
  - Ref: <https://boltless.me/posts/neovim-config-without-plugins/#nvim-lspconfig>
  - I'll have to define custom commands myself: <https://github.com/neovim/neovim/issues/28329>
- Better folding
- Separate `nix` and `main` branch:
  - Remove custom values in options/vars.lua
  - nix branch:
    - Only nord theme
    - Use custom-built Neovim package with Nix and vimPlugins (<https://github.com/NixOS/nixpkgs/blob/nixos-unstable/pkgs/applications/editors/vim/plugins/overrides.nix#L2908>)
    - Use tabby.nvim + neo-tree.nvim
      - Rework custom recipes for neo-tree.nvim (some don't work anymore)
    - Use <https://github.com/nvim-neorocks/lz.n> to structure plugin config files
  - main branch:
    - Custom nord and onedark themes (optional catpuccin)
    - Use lazy.nvim to manage plugins
    - Use nvim-cokeline + nvim-tree.lua
- Smooth colorschemes changing (dynamic highlight groups registration via event hooks)
- Update qf* highlight groups
