{ installPythonProgram, ... }:
installPythonProgram {
  name = "basedpyright";
  packages = [
    "basedpyright"
  ];
}
