{ pkgs, ... }:
with pkgs;
writeShellApplication {
  name = "vscode-js-debug";
  runtimeInputs = [
    git
    pnpm
    nodejs-slim
  ];
  text = ''
    export INSTALL_DIR=''${XDG_DATA_HOME:-$HOME/.local/share}/nvim/programs/vscode-js-debug
    if [ ! -d "$INSTALL_DIR" ]; then
      git clone --depth 1 --single-branch https://github.com/microsoft/vscode-js-debug.git "$INSTALL_DIR"
    fi

    cd "$INSTALL_DIR" || exit 1
    git restore . && git clean -df
    git fetch --tags --prune --prune-tags
    git switch --detach "$(git describe --tags "$(git rev-list --tags --max-count=1)")"

    pnpm install --prod --ignore-scripts
    pnpm run compile -- dapDebugServer
  '';
}
