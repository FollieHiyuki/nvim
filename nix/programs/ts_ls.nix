{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "ts_ls";
  packages = [
    "typescript"
    "typescript-language-server"
  ];
}
