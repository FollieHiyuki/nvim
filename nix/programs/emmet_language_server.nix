{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "emmet_language_server";
  packages = [
    "@olrtg/emmet-language-server"
  ];
}
