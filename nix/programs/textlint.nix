{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "textlint";
  packages = [
    "textlint"
  ];
}
