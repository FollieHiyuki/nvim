{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "mdx_analyzer";
  packages = [
    "@mdx-js/language-server"
  ];
}
