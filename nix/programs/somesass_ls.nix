{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "somesass_ls";
  packages = [
    "some-sass-language-server"
  ];
}
