{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "svelte";
  packages = [
    "svelte-language-server"
  ];
}
