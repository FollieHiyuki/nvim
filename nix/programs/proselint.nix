{ installPythonProgram, ... }:
installPythonProgram {
  name = "proselint";
  packages = [
    "proselint"
  ];
}
