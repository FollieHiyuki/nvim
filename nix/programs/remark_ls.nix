{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "remark_ls";
  packages = [
    "remark-language-server"
  ];
}
