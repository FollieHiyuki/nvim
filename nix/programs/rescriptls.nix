{ pkgs, ... }:
with pkgs;
writeShellApplication {
  name = "rescriptls";
  runtimeInputs = [
    gcc
    yq-go
    dune_3
    ocaml
    ocamlPackages.cppo
    nodejs-slim
    pnpm
  ];
  text = ''
    export INSTALL_DIR=''${XDG_DATA_HOME:-$HOME/.local/share}/nvim/programs/rescriptls
    if [ ! -d "$INSTALL_DIR" ]; then
      git clone --depth 1 --single-branch https://github.com/rescript-lang/rescript-vscode.git "$INSTALL_DIR"
    fi

    cd "$INSTALL_DIR" || exit 1
    git restore . && git clean -df
    git fetch --tags --prune --prune-tags
    git switch --detach "$(git describe --tags "$(git rev-list --tags --max-count=1)")"

    dune build -p analysis

    case "$(uname)" in
      Linux)
        mkdir -p server/analysis_binaries/linux
        cp _build/default/analysis/bin/main.exe server/analysis_binaries/linux/rescript-editor-analysis.exe
        ;;
      Darwin)
        if [ "$(uname -m)" = "arm64" ]; then
          mkdir -p server/analysis_binaries/darwinarm64
          cp _build/default/analysis/bin/main.exe server/analysis_binaries/darwinarm64/rescript-editor-analysis.exe
        else
          mkdir -p server/analysis_binaries/darwin
          cp _build/default/analysis/bin/main.exe server/analysis_binaries/darwin/rescript-editor-analysis.exe
        fi
        ;;
    esac

    # Remove all the unneeded `npm i` commands
    # (don't use pnpm --ignore-scripts as we require esbuild in later steps)
    yq -i 'del(.scripts.postinstall)' package.json
    pnpm install -D

    # vscode-languageserver-types is used explicitly, but is only a transitive dependency
    yq -i ".dependencies.vscode-languageserver-types = \"^$(yq -r '.packages."node_modules/vscode-languageserver-types".version' server/package-lock.json)\"" server/package.json
    pnpm install -C server

    pnpm install -C client

    pnpm run bundle-server

    chmod 0755 server/out/cli.js
  '';
}
