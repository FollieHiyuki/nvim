{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "yamlls";
  packages = [
    "yaml-language-server"
  ];
}
