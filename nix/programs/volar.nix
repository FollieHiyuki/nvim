{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "volar";
  packages = [
    "typescript"
    "@vue/language-server"
  ];
}
