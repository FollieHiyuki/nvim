{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "vscode";
  packages = [
    "vscode-langservers-extracted"
  ];
}
