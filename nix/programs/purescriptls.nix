{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "purescriptls";
  packages = [
    "purescript-language-server"
  ];
}
