{ installPythonProgram, ... }:
installPythonProgram {
  name = "codespell";
  packages = [
    "codespell"
  ];
}
