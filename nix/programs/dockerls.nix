{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "dockerls";
  packages = [
    "dockerfile-language-server-nodejs"
  ];
}
