{ installPythonProgram, ... }:
installPythonProgram {
  name = "pylsp";
  packages = [
    "python-lsp-server[all]"
    "pylsp-mypy"
    "python-lsp-isort"
    "python-lsp-black"
    "pylsp-rope"
  ];
}
