{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "bashls";
  packages = [
    "bash-language-server"
  ];
}
