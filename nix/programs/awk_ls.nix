{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "awk_ls";
  packages = [
    "awk-language-server"
  ];
}
