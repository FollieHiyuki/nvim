{ installPythonProgram, ... }:
installPythonProgram {
  name = "debugpy";
  packages = [
    "debugpy"
  ];
}
