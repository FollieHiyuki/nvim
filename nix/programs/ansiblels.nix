{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "ansiblels";
  packages = [
    "@ansible/ansible-language-server"
  ];
}
