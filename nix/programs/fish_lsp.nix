{ pkgs, ... }:
with pkgs;
writeShellApplication {
  name = "fish_lsp";
  runtimeInputs = [
    gnused
    tree-sitter
    yarn-berry
    nodejs-slim
  ];
  text = ''
    export INSTALL_DIR=''${XDG_DATA_HOME:-$HOME/.local/share}/nvim/programs/fish_lsp
    if [ ! -d "$INSTALL_DIR" ]; then
      git clone --depth 1 --single-branch https://github.com/ndonfris/fish-lsp.git "$INSTALL_DIR"
    fi

    cd "$INSTALL_DIR" || exit 1
    git restore . && git clean -df
    git fetch --tags --prune --prune-tags
    git switch --detach "$(git describe --tags "$(git rev-list --tags --max-count=1)")"

    sed -i'.bak' '/"tsc":/d' package.json
    yarn install --mode=skip-build
    yarn run compile

    chmod 0755 out/cli.js

    cd node_modules/tree-sitter-fish/
    tree-sitter build -w -o "$INSTALL_DIR"/tree-sitter-fish.wasm .
  '';
}
