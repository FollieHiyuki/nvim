{ pkgs, ... }:
with pkgs;
writeShellApplication {
  name = "elixirls";
  runtimeInputs = [
    git
    elixir
  ];
  text = ''
    export INSTALL_DIR=''${XDG_DATA_HOME:-$HOME/.local/share}/nvim/programs/elixirls
    export LC_ALL=C.UTF-8
    export MIX_ENV=prod

    if [ ! -d "$INSTALL_DIR" ]; then
      git clone --depth=1 --single-branch https://github.com/elixir-lsp/elixir-ls.git "$INSTALL_DIR"
    fi

    cd "$INSTALL_DIR" || exit 1
    git restore . && git clean -df
    git fetch --tags --prune --prune-tags
    git switch --detach "$(git describe --tags "$(git rev-list --tags --max-count=1)")"

    mix deps.get
    mix compile
    mix elixir_ls.release2 -o release/
  '';
}
