{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "tailwindcss";
  packages = [
    "@tailwindcss/language-server"
  ];
}
