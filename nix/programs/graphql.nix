{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "graphql";
  packages = [
    "graphql-language-service-cli"
  ];
}
