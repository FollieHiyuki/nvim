{ installPythonProgram, ... }:
installPythonProgram {
  name = "sqlfluff";
  packages = [
    "sqlfluff"
  ];
}
