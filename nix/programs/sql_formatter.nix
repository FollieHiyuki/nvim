{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "sql_formatter";
  packages = [
    "sql-formatter"
  ];
}
