{ installNodeJSProgram, ... }:
installNodeJSProgram {
  name = "cspell";
  packages = [
    "bash-language-server"
  ];
}
