_: {
  perSystem =
    { lib, pkgs, ... }:
    let
      # TODO: make this derivation independent of the Nix store
      # (avoid symlinking `python3` to venv from Nix store somehow)
      installPythonProgram =
        { name, packages }:
        with pkgs;
        writeShellApplication {
          inherit name;
          runtimeInputs = [
            python3
          ];
          text = ''
            export INSTALL_DIR=''${XDG_DATA_HOME:-$HOME/.local/share}/nvim/programs/${name}
            python3 -m venv "$INSTALL_DIR"
            "$INSTALL_DIR"/bin/pip install \
              --require-virtualenv \
              --isolated \
              --compile \
              --upgrade \
              --check-build-dependencies \
              ${lib.escapeShellArgs packages}
          '';
        };

      installNodeJSProgram =
        { name, packages }:
        with pkgs;
        writeShellApplication {
          inherit name;
          runtimeInputs = [
            pnpm
            nodejs-slim
          ];
          text = ''
            export INSTALL_DIR=''${XDG_DATA_HOME:-$HOME/.local/share}/nvim/programs/${name}
            mkdir -p "$INSTALL_DIR"
            pnpm add -C "$INSTALL_DIR" ${lib.escapeShellArgs (builtins.map (x: "${x}@latest") packages)}
          '';
        };

      programs =
        with lib;
        mapAttrs'
          (
            name: _:
            nameValuePair (removeSuffix ".nix" name) (
              import ./programs/${name} {
                inherit pkgs installPythonProgram installNodeJSProgram;
              }
            )
          )
          (
            filterAttrs (name: type: type == "regular" && hasSuffix ".nix" name) (builtins.readDir ./programs)
          );
    in
    with lib;
    {
      apps = mapAttrs' (
        name: drv:
        nameValuePair "install/${name}" {
          type = "app";
          program = drv;
        }
      ) programs;
    };
}
