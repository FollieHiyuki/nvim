inputs@{ flake-parts, treefmt-nix, ... }:
flake-parts.lib.mkFlake { inherit inputs; } {
  systems = [
    "x86_64-linux"
    "x86_64-darwin"
    "aarch64-linux"
    "aarch64-darwin"
  ];

  imports = [
    treefmt-nix.flakeModule
    ./program-install.nix
  ];

  perSystem =
    {
      system,
      config,
      lib,
      pkgs,
      ...
    }:
    {
      treefmt = {
        projectRootFile = "flake.lock";
        settings.formatter.stylua.includes = [
          "*.lua"
          ".stylua.toml"
        ];
        programs = builtins.listToAttrs (
          builtins.map
            (x: {
              name = x;
              value.enable = true;
            })
            [
              "nixfmt"
              "deadnix"
              "statix"
              "stylua"
            ]
        );
      };

      devShells.default =
        with pkgs;
        mkShellNoCC {
          name = "nvim-devShell";
          inputsFrom = [ config.treefmt.build.devShell ];
          buildInputs = [
            lua-language-server
            nil
          ];
        };
    };
}
